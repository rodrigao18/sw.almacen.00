//funcion para buscar productos
function validarProductoExiste(e) {
    if (e.keyCode === 13 && !e.shiftKey) {
        var buscar = $("#buscarCodigoBarra").val(); 
        if (buscar.trim() == "") {            
            return;
        }
        if (isNaN(buscar)) {
			document.getElementById('buscarCodigoBarra').value = '';
			console.log('error');
			return;
		}

        if (!isNaN(buscar)) {
            var sql = 'select count(*) as existe from productos where codigo_barra ="' + buscar + '"';
        }
        console.log(sql);

        $.ajax({
            type: 'POST',
            url: 'php/consulta.php',
            data: {
                sql: sql,
                tag: 'array_de_datos'
            },           
            success: function (data) {
                var arreglo = JSON.parse(data);
                var existe = arreglo[0][0];
                if (existe == 1) {
                    buscarProductos(buscar);
                } else {
                    console.log('no existe el producto');
                    document.getElementById('buscarCodigoBarra').value = '';
                }
            },
            error: function (request, status, error) {
                console.error("Error: Could not buscarProductos");
            }

        });
    }
}

function buscarProductos(buscar) {

    $("#salidaProducto").html("");   
        
        document.getElementById('btnAgregar').disabled = false;       

        if (!isNaN(buscar)) {
            var sql = 'SELECT id,codigo_barra,nombre,stock,precio_compra,precio_venta FROM PRODUCTOS where codigo_barra = "' + buscar.trim() + '"';
        }
        console.log(sql);

        $.ajax({
            type: 'POST',
            url: 'php/consulta.php',
            data: {
                sql: sql,
                tag: 'array_de_datos'
            },
            beforeSend: function (objeto) {
                $("#salidaProducto").html("<img src='imagenes/ajax-loader.gif'>");
            },
            success: function (data) {
                var arreglo = JSON.parse(data);
                $("#salidaProducto").html("");
                cargarDatostablaProductos(arreglo);
                document.getElementById('btnAgregar').focus;
            },
            error: function (request, status, error) {
                console.error("Error: Could not buscarProductos");
            }

        });
    }


function cargarDatostablaProductos(arreglo) {


    if(arreglo.length >0) {
        $("#salidaProducto").append(
        '<input type="hidden"  value="' + arreglo[0]['id'] + '" id="idProductoBuscado">' +
        '<table  class="table table-striped" >' +
        '<thead>' +
        '<tr>' +
        '	<th scope="col" width="10%"> Código</th>' +
        '	<th scope="col" width="35%"> Nombre</th>' +
        '	<th scope="col" width="20%"> Stock anterior</th>' +
        '	<th scope="col" width="5%"> </th>' +
        '	<th scope="col" width="20%"> </th>' +
        '</tr>' +
        '</thead>' +
        '<tbody id="tablaBody"></tbody>' +
        '</table>');

        $("#tablaBody").append('<tr>' +
            '<td>' + arreglo[0]['codigo_barra'] + '</td>' +
            '<td>' + arreglo[0]['nombre'] + '</td>' +
            '<td>' + arreglo[0]['stock'] + '</td>' +
            '<td><strong>  + </strong> </td>' +
            '<td>' + '<input type="number" class="form-control" id="stockAgregar" min="0" onkeyup="validarNumero(this.value)" value=1 placeholder="Ingrese stock">' + '</td>' +
            '</tr>');
    }else{
        $("#salidaProducto").append("No se encontro producto asociado a este codigo de barra");

    }
    comprobarProductoRepetido();

}
//*-comprobar si hay un rpducto repetido
//*-en la tabla temporal
function comprobarProductoRepetido() {
    var codigo = 0;
    var codigoTemp = 0;
    var tablaC = document.getElementById('tablaBody'),
        rIndex;
    //*-codigo de barras de la primera tabla
    codigo = tablaC.rows[0].cells[0].innerText;
    
    var tablaTemp=document.getElementById('tablaBodyInventario'),
        rIndex;
    var nFilas = $("#tablaBodyInventario > tr").length;

    //*-recorro la segunda tabla buscando  codigo repetido
    for (var i = 0; i < nFilas; i++) { 
        codigoTemp = tablaTemp.rows[i].cells[1].innerText;
        if (codigo == codigoTemp) { 
            swal('Advertencia', 'ya ingreso este producto', 'warning');
            $("#salidaProducto").html('');
            document.getElementById('buscarCodigoBarra').value = '';
            document.getElementById('btnAgregar').disabled = true;
            return;
        }
    }

 }
//*-validar si el Numero del stock es negativo;
function validarNumero(numero) {
     if (numero < 0) {
        document.getElementById('stockAgregar').value = 0;
     }    
}
//*-agregar stock
function agregarStock(e) {
    e.preventDefault();
    if ($('#buscarCodigoBarra').val()=='') {
        swal('Precaucion', 'debe ingresar un codigo de barras valido', 'warning');
        document.getElementById('btnAgregar').disabled = true;
    }
    else { 
        var id = $("#idProductoBuscado").val();
        var stockAgregar = $("#stockAgregar").val();
        var sql = 'UPDATE productos set stock=stock+' + stockAgregar + ' WHERE id=' + id;
        console.log(sql);
        $.ajax({
            type: 'POST',
            url: 'php/consulta.php',
            data: {
                sql: sql,
                tag: 'crud_productos'
            },       
            success: function (data) {
                console.error(data);
                var arreglo = JSON.parse(data);
                $("#buscarCodigoBarra").val("");
              //  $("#salidaProducto").html("Se agrego el stock correctamente al producto");
                document.getElementById('btnAgregar').disabled = true;          
                insertNewRegistro();
    
            },
            error: function (request, status, error) {
                console.error("Error: Could not buscarProductos");
            }
    
        });
    }
   

}
//*-ingresa un registro al tabla inventario
function insertNewRegistro() {   
    var idInventario = document.getElementById('idInventario').value;
    if (idInventario == 0) {
        
        var sql = 'INSERT INTO inventario (fecha_inventario,id_vendedor,id_turno,estado)VALUES(NOW(),' + ID_VENDEDOR + ',' + ID_TURNO + ',1);'
        console.log(sql);
        $.ajax({
            type: 'POST',
            url: 'php/consulta.php',
            data: {
                tag: 'insert_return_id',
                sql: sql
            },
            success: function (data) {
                console.log(data);
                if (!isNaN(data)) {
                    $.notify({
                        title: "Nuevo registro : ",
                        message: "Se ha Creado un nuevo registro de inventario n° " + data,
                        icon: 'fas fa-check'
                    }, {
                        type: "info",
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        offset: 70,
                        spacing: 70,
                        z_index: 1031,
                        delay: 1000,
                        timer: 2000
                    });
                    document.getElementById('idInventario').value = data;
                    insertarProductos(data);
                } else {
                    console.error("No es correto");
                }
            },
            error: function (request, status, error) {
                console.error("Error: Could not finalizar venta");
            }
        });

    } else { 
       var idInventario =  document.getElementById('idInventario').value; 
        insertarProductos(idInventario); 
    }

}

function insertarProductos(lastId) { 
    var codigo = 0;
    var nombre;
    var nFilas = $("#tablaBody > tr").length;
    var tablaC = document.getElementById('tablaBody'),
  rIndex; 
    console.log('nFilas ' + nFilas);
  
        codigo = tablaC.rows[0].cells[0].innerText;
        nombre = tablaC.rows[0].cells[1].innerText;
		cantidad = document.getElementById('stockAgregar').value;
		console.error( cantidad);
	
        var sql = 'insert into inventario_relacional (codigo_barra,id_inventario,cantidad_ingresada) VALUES ("' + codigo + '",' + lastId + ',' + cantidad + ')';
        console.log(sql);
        $.ajax({
            type: 'POST',
            url: 'php/consulta.php',
            data: {
                tag: 'crud_productos',
                sql: sql
            },
            success: function (data) {
                addProductoTemp();
                $.notify({
                    title: "Stock actualizado : ",
                    message: "El producto fue agregado al informe inventario n° " + lastId,
                    icon: 'fas fa-check'
                }, {
                    type: "success",
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    offset: 70,
                    spacing: 70,
                    z_index: 1031,
                    delay: 1000,
                    timer: 2000
                });
            },
            error: function (request, status, error) {
                console.error("Error: Could not finalizar venta");
            }
        });
     

}

//*-agrego lops productos a una tabla temporal;
function addProductoTemp() {    
    var codigo = 0;
    var nombre;
    var stockAnterior = 0;
    var cantIngre = 0;
    var stockActual = 0;
    var tablaC = document.getElementById('tablaBody'),
        rIndex;
    codigo = tablaC.rows[0].cells[0].innerText;
    nombre = tablaC.rows[0].cells[1].innerText;
    stockAnterior = tablaC.rows[0].cells[2].innerText;
    cantIngre = document.getElementById('stockAgregar').value;
    stockActual = parseInt(stockAnterior) + parseInt(cantIngre);   

    $("#tablaBodyInventario").append('<tr id="fila">' +
    '<td></td>' +
    '<td>' + codigo + '</td>' +
    '<td>' + nombre + '</td>' +
    '<td>' + stockAnterior + '</td>' +
    '<td>'+ cantIngre +'</td>' +
    '<td>' + stockActual + '</td>' +    
        '</tr>');
    agregarNumeracionItem();
    //*-limpar salida de tabla
    $("#salidaProducto").html("");
}


//FUNCTION PARA AGREGAR UN ITEM A LA VENTA NUEVA Y RESETEAR CUANDO SE BORRE UN ITEM;
function agregarNumeracionItem() {
	var tabla = document.getElementById("tablaBodyInventario"),
		rIndex;
	var nFilas = $("#tablaBodyInventario > tr").length;
	for (var i = 0; i < nFilas; i++) {
		tabla.rows[i].cells[0].innerHTML = i + 1;
	}

}