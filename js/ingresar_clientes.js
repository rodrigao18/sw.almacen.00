function GuardarCliente(e){
	e.preventDefault();

	if ($("#nombre").val() == "" || $("#rutCliente").val() == "" || $("#direccion").val()=="") {
		swal("Advertencia", "ingrese rut , direccion o nombre del cliente", "warning");

	}else{
		var obs=document.getElementById('observaciones').value;
		var rut=document.getElementById('rutCliente').value;
		var nombre=document.getElementById('nombre').value;
		var direccion=document.getElementById('direccion').value;
		var correo=document.getElementById('correo').value;
		var celular=document.getElementById('celular').value;
		var fono=document.getElementById('fono').value;
		var sql =  'INSERT INTO CLIENTES (rut,nombre,direccion,correo,celular,fono,observacion) '+
		'	VALUES ("'+rut+'","'+nombre+'","'+ direccion +'","'+correo+'","'+celular+'","'+ fono +'","'+ obs +'")';

		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			async: true,
			data: {
				tag: 'crud_productos',
				sql: sql
			},
			success: function (data) {
				console.log(data);
				if (data == 1) {
					swal("Insert!", "Datos ingresados correctamente!", "success");
					//window.location.href = "ver_vendedores.php";
				} else {
					alert('Error En El Ingreso');
				}
			},
			error: function (request, status, error) {
				console.error("Error: Could not insertar vendedores");
			}
		});
	}

}


function mesg() {
	estado = document.getElementById("switch-id").checked;
	console.log(estado);
	if (estado == false) {
		swal("Advertencia!", "desea inhabilitar este cliente, no se prodra manipular si lo hace!", "warning");
	}
}



//*-comprobar si cliente ya existe
function comprobarCliente() {	
	if($("#rutCliente").val() != ""	){
		var rutCliente = $("#rutCliente").val();
		console.error(rutCliente);

		var sql = 'SELECT count(*) FROM clientes where rut=' + rutCliente;
	
		//-*AJAX	
		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: { sql: sql, tag: 'array_de_datos' },

			success: function (data) {
				var arreglo = JSON.parse(data);
				existe = arreglo[0][0];
				if (existe < 1) {
			
					} else {
						$.notify({
							title: "Rut existente : ",
							message: "El rut de estes cliente ya existe en la base de datos:",
							icon: 'fas fa-exclamation-circle'
						}, {
							type: "danger",
							placement: {
								from: "top",
								align: "right"
							},
							offset: 70,
							spacing: 70,
							z_index: 1031,
							delay: 1000,
							timer: 1000
						});
					document.getElementById('rutCliente').focus();
					$("#rutCliente").val(convertirRut(rutCliente));	
					}
				
			},
			error: function (request, status, error) {
				console.error("Error: Could not comprobarCLiente");
			}
		});

	}else{
		var rutCliente = $("#rutCliente").val();
		console.error(rutCliente);
		
	}
	
	return;
}