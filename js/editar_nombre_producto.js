/*------------------------ Editar descripcion -----------------------*/
var editando = false;
//funcion para tranformar una celda editable
function transformarEnEditable(nodo) {

	//El nodo recibido es SPAN

	if (editando == false) {
		var nodoTd = nodo.parentNode; //Nodo TD
		var nodoTr = nodoTd.parentNode; //Nodo TR
		var nodosEnTr = nodoTr.getElementsByTagName('td');
		var nombre = nodoTd.textContent;

		//var nuevoCodigoHtml = '<td> <input type="text" id="nombre" value="' + nombre + '" style="width:100%;" onkeypress="pulsar(event,this)"></td>';
		var nuevoCodigoHtml = '<td> <textarea type="text" rows="3" id="nombre" style="width:100%;" onkeyup="salir(event,this)" onkeypress="pulsar(event,this)">' + nombre + '</textarea>  </td>';
    console.warn(nuevoCodigoHtml);
		nodoTd.innerHTML = nuevoCodigoHtml;


		editando = "true";
	} else {
		alert('Solo se puede editar una línea. Recargue la página para poder editar otra');

	}

}

function pulsar(e, nodo) {


	if (e.keyCode === 13 && !e.shiftKey) {
		e.preventDefault();

		var nodoTd = nodo.parentNode; //Nodo TD
		var nodoTr = nodoTd.parentNode; //Nodo TR
		var nodoContenedorForm = document.getElementById('contenedorForm'); //Nodo DIV
		var nodosEnTr = nodoTr.getElementsByTagName('td');
    var nombreProducto = document.getElementById('nombre').value;
    var idVentaR = nodosEnTr[0].textContent;		
		var nombreEditable = document.getElementById('nombre').value;    
		
		$.notify({
			title: "Nombre modificado : ",
			message: "Se actualizó la información de la venta",
			icon: 'fas fa-check'
		}, {
				type: "info",
				placement: {
					from: "top",
					align: "right"
				},
				offset: 70,
				spacing: 70,
				z_index: 1031,
				delay: 1000,
				timer: 2000
			});

    updateProductoModificado(nombreProducto,idVentaR);


		var nuevoCodigoHtml = '<td> <span class="editar" onclick="transformarEnEditable(this)"> ' + nombreEditable + '</span> </td>';
		nodoTd.innerHTML = nuevoCodigoHtml;
		editando = false;
	}
}
//*-salir de la celda editable si no deseo editar
function salir(event, nodo){
  if (event.code === 'Escape' || event.keyCode === 27) {
	console.log('escape');
	event.preventDefault();

	var nodoTd = nodo.parentNode; //Nodo TD
	var nodoTr = nodoTd.parentNode; //Nodo TR
	var nodoContenedorForm = document.getElementById('contenedorForm'); //Nodo DIV
	var nodosEnTr = nodoTr.getElementsByTagName('td');
	var nombreProducto = document.getElementById('nombre').value;
	var idVentaR = nodosEnTr[0].textContent;		
	var nombreEditable = document.getElementById('nombre').value;
	var nuevoCodigoHtml = '<td> <span class="editar" onclick="transformarEnEditable(this)"> ' + nombreEditable + '</span> </td>';

	nodoTd.innerHTML = nuevoCodigoHtml;
	editando = false;
	  }
}