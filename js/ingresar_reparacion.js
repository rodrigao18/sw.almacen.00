var ID;


//*-comprobar si cliente ya existe
function cargarDatos(id){
	ID=id;
	var sql ='SELECT id,rut,nombre,direccion,celular FROM CLIENTES WHERE id='+id;

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			id: id,
			tag: 'array_de_datos',
			sql: sql
		},
		success: function (data) {

			console.log(data);
			var arreglo = JSON.parse(data);
			document.getElementById('rutCliente').value=arreglo[0]['rut'];
			document.getElementById('nombre').value=arreglo[0]['nombre'];
			document.getElementById('direccion').value=arreglo[0]['direccion'];
			document.getElementById('celular').value=arreglo[0]['celular'];
			cargar_tecnicos();
		},
		error: function (request, status, error) {
			alert("Error: Could not editar vendedores");
		}
	});


}

function ingresar_Reparacion(e){
	e.preventDefault();
	var fecha_ingreso = document.getElementById('fecha_inicio').value;	
	var falla=document.getElementById('falla').value;
	var entrega=document.getElementById('entrega').value;
	var tecnico=document.getElementById('select_tecnicos').value;
	var estado=document.getElementById('select_estado').value;
	var sql = 'insert into reparaciones (fecha,id_cliente,id_tecnico,falla,entrega,estado)' +
		'VALUES("' + fecha_ingreso + '",' + ID + ',' + tecnico + ',"' + falla + '", '+
		' '  + entrega + ','  + estado + ')';
	console.error(sql);

	

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		async: true,
		data: {
			tag: 'insert_return_id',
			sql: sql
		},
		success: function (data) {
			
			if (!isNaN(data)) {
				reparaciones_relacional(data);				
			} else {
				swal("Error", 'Revice el nombre del producto las pulgadas deber ser por ejemplo (14"") ', "error");	 
			}
		},
		error: function (request, status, error) {
			console.error("Error: Could not  guardarProducto");
		}
	});


	}
function reparaciones_relacional(id_reparacion){

	serie = document.getElementById('serie').value;	
	tipo=document.getElementById('tipo').value;
	marca=document.getElementById('marca').value;
	modelo=document.getElementById('modelo').value;
	accesorios=document.getElementById('accesorios').value;


	var sql = 'insert reparaciones_relacional  (tipo,marca,modelo,serie,accesorios,mano_obra,fecha_entrega,repuesto,saldo,id_reparacion) ' +
	' VALUES ("' + tipo + '","' + marca + '","' + modelo + '","' + serie + '","' + accesorios + '",NULL,NULL,NULL,NULL,'+id_reparacion+')';
	
	console.log(sql);
	
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		async: false,
		data: {
			tag: 'crud_productos',
			sql: sql
		},
		success: function (data) {		
			swal("Insert!", "La reparacion fue ingresada correctamente!", "success");	
			
		},
		error: function (request, status, error) {
			console.error("Entro aca");
		}

	});



}


//************************* */
function cargar_tecnicos() {

	var sql = 'SELECT id,nombre FROM tecnicos';
	//AJAX	
	$.ajax({
	type: 'POST',
	url:  'php/consulta.php', 
	data: {sql: sql,tag: 'tecnicos'},
			
	success:function (data){
				$('#select_tecnicos').html(data).fadeIn();
				$('#select_estado option[value="1"]').attr("selected", true);  
				
	},
	error: function (request, status, error)
				{alert('Error: Could not categoria');
	}
	})
}

function comprobarCliente() {	
	if($("#rutCliente").val() != ""	){
		var rutCliente = $("#rutCliente").val();
		console.error(rutCliente);

		var sql = 'SELECT count(*) FROM clientes where rut=' + rutCliente;
	
		//-*AJAX	
		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: { sql: sql, tag: 'array_de_datos' },

			success: function (data) {
				var arreglo = JSON.parse(data);
				existe = arreglo[0][0];
				if (existe < 1) {
			
					} else {
						$.notify({
							title: "Rut existente : ",
							message: "El rut de estes cliente ya existe en la base de datos:",
							icon: 'fas fa-exclamation-circle'
						}, {
							type: "danger",
							placement: {
								from: "top",
								align: "right"
							},
							offset: 70,
							spacing: 70,
							z_index: 1031,
							delay: 1000,
							timer: 1000
						});
					document.getElementById('rutCliente').focus();
					$("#rutCliente").val(convertirRut(rutCliente));	
					}
				
			},
			error: function (request, status, error) {
				console.error("Error: Could not comprobarCLiente");
			}
		});

	}else{
		var rutCliente = $("#rutCliente").val();
		console.error(rutCliente);
		
	}
	
	return;
}