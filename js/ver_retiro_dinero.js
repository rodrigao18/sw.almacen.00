//*-cargar datos mediante async wait()
let cargarRetiro= async () => { 
	const baseUrl = 'php/consultaFetch.php';
    let consulta=`SELECT id,dinero,observacion,tipo_turno 
    FROM retiro`;
	
	const sql = {sql: consulta, tag: `array_datos`} 
	
	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();
		//*-se parsea solo la respuesta del Json enviada por el servidor.
		let array = JSON.parse(data);		
		console.log(array);		
		tablaRetiro(array);
		//*-promesa de la funcion denguaje la ejecuto a la espera
		//*-de la respuesta del servidor.	
		const botones = await lenguaje();	
		
	} catch (error) {
		console.log('error en la conexion ', error);
	}	
}


//*-productos
let tablaRetiro = (arreglo) => {
	let tbody = document.getElementById('tablaBody');
	var EstadoColumna; 
	for (let i of arreglo) {
		console.error('');
		if(i['tipo_turno']==1){
			EstadoColumna = "<span class='badge badge-info'>Mañana</span>";
		}else if(i['tipo_turno']==2){
			EstadoColumna = "<span class='badge badge-dark'>Tarde</span>";
		}else if(i['tipo_turno']==0){
			EstadoColumna = "<span class='badge badge-success'>Sin turno</span>";
		}
		
		tbody.innerHTML +=
		`<tr>		   
		   <td>${i['id']}</td>
		   <td>${i['dinero']}</td>
		   <td>${i['observacion']}</td>		
		   <td>${EstadoColumna}</td>			
		   <td><form method="POST" action="editar_retiro.php">
		   <button type="submit" class="btn btn-secondary" data-toggle="tooltip"
			data-placement="top" title="Editar" name="id" value=${i['id']}><i class="fas fa-edit" aria-hidden="true"></i></button></form></td>			
			<td ><button class="btn  btn-danger" data-toggle="tooltip" data-placement="top" title="Borrar" onclick=eliminarRetiro(event,${i['id']})><i class="fa fa-trash" aria-hidden="true"></i></button></td>			
		 </tr>`
	 	
	}
	$('[data-toggle="tooltip"]').tooltip();

 }



function lenguaje() {

	var f = new Date();
	var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();

	var table=$('#tablaProductos').DataTable({

		language: {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			"infoFiltered": "(Filtrado de _MAX_ total entradas)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Entradas",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		},
		"aria": {
			"sortAscending": ": activate to sort column ascending",
			"sortDescending": ": activate to sort column descending"
		},
		"order": [[1, "desc"]],
		"stateSave":true
	});


     new $.fn.dataTable.Buttons(table, {
		buttons: [
			{
				extend: 'excelHtml5',
				title: 'ver_reparaciones' + fecha + ''
            }, {
				extend: 'pdfHtml5',
				title: 'ver_reparaciones' + fecha + ''
            }]

	});

	table.buttons(0, null).container().prependTo(
		table.table().container()
	);


}


window.onload = cargarRetiro