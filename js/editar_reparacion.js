var ID;

function cargarReparacion(id){
    ID=id;

    var sql = 'SELECT DATE(fecha) as fecha ,id_tecnico,falla,entrega,estado,tipo,marca,modelo,serie,accesorios '+
    ' FROM reparaciones r INNER JOIN reparaciones_relacional rr ON rr.id_reparacion=r.id  WHERE r.id='+id;

    $.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			id: id,
			tag: 'array_de_datos',
			sql: sql
		},
		success: function (data) {
            var arreglo = JSON.parse(data);
            console.error(arreglo);
            var id_tecnico=arreglo[0]['id_tecnico']
            cargarTecnico(id_tecnico);
			cargarDatos(arreglo);		
		},
		error: function (request, status, error) {
			alert("Error: Could not editar vendedores");
		}
	});

}

function cargarTecnico(id){

    //SQL SELECT;
		var sql = 'SELECT id, nombre FROM tecnicos';
	
		//AJAX;
		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: {sql: sql,tag: 'tecnicos'},
					 
			success: function (data){    
						$('#select_tecnicos').html(data).fadeIn();
		//DEJAR SELECCIONADO EL PROVEEDOR CON EL ID SELECCIONADO SACADO DESDE LA FUNCION LLAMADA A LA BASE;  
						$('#select_tecnicos option[value="' + id + '"]').attr("selected", true);
						   
			},
			error:function (request, status, error)
						{alert('Error: Could not categoria');}
			})

}
function cargarDatos(arreglo){
	document.getElementById('fecha_inicio').value=arreglo[0]['fecha'];
	document.getElementById('serie').value=arreglo[0]['serie'];
	document.getElementById('tipo').value=arreglo[0]['tipo'];
	document.getElementById('marca').value=arreglo[0]['marca'];
	document.getElementById('modelo').value=arreglo[0]['modelo'];
	document.getElementById('accesorios').value=arreglo[0]['accesorios'];
	document.getElementById('falla').value=arreglo[0]['falla'];
	document.getElementById('entrega').value=arreglo[0]['entrega'];
	var est=arreglo[0]['estado'];
	console.error('est' + est);
	if(est==1){
		$('#select_estado option[value="1"]').attr("selected", true);
	}else if(est==2){
		$('#select_estado option[value="2"]').attr("selected", true);
	}
	else if(est==3){
		$('#select_estado option[value="3"]').attr("selected", true);
	}

}

function editarReparaciones(e){
	e.preventDefault();

	var serie=document.getElementById('serie').value;
	var tipo=document.getElementById('tipo').value;
	var marca=document.getElementById('marca').value;
	var modelo=document.getElementById('modelo').value;
	var accesorios=document.getElementById('accesorios').value;
	var falla=document.getElementById('falla').value;
	var entrega=document.getElementById('entrega').value;
	var mano=document.getElementById('obra').value;
	var repuesto=document.getElementById('repuesto').value;
	var saldo=document.getElementById('saldo').value;
	var fecha_entrega=document.getElementById('fecha_entrega').value;
	var estado=document.getElementById('select_estado').value;


		//SQL UPDATE;
		var sql = 'UPDATE reparaciones p INNER JOIN reparaciones_relacional pr ON p.id=pr.id_reparacion set pr.serie = "' + serie + '" ,pr.tipo = "' + tipo + '" ,p.estado = ' + estado + ', ' +
		' pr.marca  = "' + marca + '" , pr.modelo = "' + modelo + '",pr.accesorios = "' + accesorios + '",' +
		' p.falla ="' + falla + '",p.entrega ="' + entrega + '", pr.mano_obra = "' + mano + '", pr.fecha_entrega = "' + fecha_entrega + '",pr.repuesto = "' + repuesto + '", ' +
		' pr.saldo = ' + saldo + '  WHERE p.id='+ID;

		 console.error(sql);
		 
		 $.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: {
				sql: sql, tag: 'crud_productos' },
			success: function (data) {
				console.log(data);
				if (data == 1) {
				
					swal("Update!", "Datos actualizados correctamente!", "success");
					
				} else {
					swal("Error", 'No se pudo actualizar los datos ', "error");	 
				}
			},
			error: function (request, status, error) {
				alert("Error: Could not editarProducto");
			}
		});

}