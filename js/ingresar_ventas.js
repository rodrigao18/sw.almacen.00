var ITEM = 0; //VARIABLE GLOBAL PARA ASIGNAR UN NUMERO AL ITEM;
var CODIGOPROMOCION; //VARIABLE GLOBAR PARA EL CODIGO DE LA PROMOCIÓN;
var ARRPRODUCTOS;
//funcion para buscar productos por codigo
function buscarProductos(e) {
	
	$("#salidaProducto").html("");
	if (e.keyCode === 13 && !e.shiftKey) {

		var buscar = $("#buscarCodigoBarra").val();
		//*-solo numeros.
		if (isNaN(buscar)) {
			document.getElementById('buscarCodigoBarra').value = '';
			console.log('error');
			return;
		}
		if (buscar.trim() == "") {
			swal("Precaución", "Es necesario ingresar un codigo de barras", "warning");
			return;
		}

		if (!isNaN(buscar)) {
			var sql = 'SELECT p.id,codigo_barra ,nombre,precio_instalacion,precio_mayorista,precio_venta,sb.stockb1,sb.stockb2 '+
			' FROM PRODUCTOS p INNER JOIN stock_bodegas sb ON sb.id_producto=p.id WHERE codigo_barra= "' + buscar.trim() + '"';
		}
		
	
		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: {
				sql: sql,
				tag: 'array_de_datos'
			},
			beforeSend: function (objeto) {
				$("#salidaProducto").html("<img src='imagenes/ajax-loader.gif'>");
			},
			success: function (data) {
				var arreglo = JSON.parse(data);
				ARRPRODUCTOS=arreglo;
				$("#salidaProducto").html("");
				let precio_instalacion = document.getElementById('precioInstalacion').value=arreglo[0]['precio_instalacion'];
				let precio_mayorista = document.getElementById('precioMayorista').value=arreglo[0]['precio_mayorista'];
				let precio_venta = document.getElementById('precioVenta').value=arreglo[0]['precio_venta'];
				document.getElementById('nombreProducto').innerHTML='<h3>' +arreglo[0]['nombre'] + '</h3>';
			
			//	cargarPromocion(buscar, arreglo);
			if (arreglo.length > 0) {
				if (arreglo[0]['stockb1'] <= 2 && arreglo[0]['stockb2'] <= 2 ) {
					console.error('stock minino');
					swal("Advertencia", "El stock es critico es = " + arreglo[0]['stock'], "warning");
				//	cargarDatostablaProductos(arreglo);
				} else {
				//	cargarDatostablaProductos(arreglo);
					let preciosocultos = document.getElementById('datos-productos').style.opacity = "1";
					document.getElementById('checkM').disabled=false;
					document.getElementById('checkI').disabled=false;
					document.getElementById('checkV').disabled=false;
					document.getElementById('stockb1').value=arreglo[0]['stockb1'];
					document.getElementById('stockb2').value=arreglo[0]['stockb2'];
				}
			}

			},
			error: function (request, status, error) {
				console.error("Error: Could not buscarProductos");
			}

		});
	}
}

//BUSQUEDA DE PRODUCTOS POR NOMBRE
function prebusqueda(estado){
	clearTimeout($.data(this, 'timer'));
	if(estado=="detalle"){
		 var wait = setTimeout(buscarProductospornNombres, 750);
	}
	$(this).data('timer', wait);

}
//FIN DE LA FUNCION


//funcion para buscar productos
function buscarProductospornNombres() {
	$("#tablaProductos").show();
	$("#salidaTabla").html("");

	var buscar = $("#buscar").val();
	if(buscar=="" || buscar == " "){return;}
	if( buscar.indexOf(" ") !== -1){
		console.error('aca');
	}
	if(isNaN(buscar) || buscar.indexOf(" ") !== -1){
		var sql = 'SELECT id,codigo_barra,nombre FROM PRODUCTOS where nombre LIKE "%' + buscar + '%" ';
	}else{
		var sql = 'SELECT id,codigo_barra,nombre FROM PRODUCTOS where nombre LIKE "%' + buscar + '%" ';
	}

	/*var sql = 'SELECT p.id,codigo_barra ,nombre,precio_instalacion,precio_mayorista,precio_venta,sb.stockb1,sb.stockb2 '+
			' FROM PRODUCTOS p INNER JOIN stock_bodegas sb ON sb.id_producto=p.id WHERE codigo_barra= "' + buscar.trim() + '"';*/

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			sql: sql,
			tag: 'array_de_datos'
		},
		beforeSend: function (objeto) {
			$("#salidaTabla").html("<img src='imagenes/ajax-loader.gif'>");
		},
		success: function (data) {
			$("#salidaTabla").html("");
			var arreglo = JSON.parse(data);
			console.error(arreglo);
			tablaProductos(arreglo);
			$("#tablaResumen").hide();

		},
		error: function (request, status, error) {
			console.error("Error: Could not buscarProductos");
		}
	});
}
//TABLA BUSQUEDA DE PRODUCTOS
function tablaProductos(arreglo) {
	$("#salidaTabla").append('<button class="btn btn-sm btn-info float-left" onclick = regresar(event)  data-toggle="tooltip" data-placement="top" title="" data-original-title="Regresar a resumen" ><i class="fas fa-chevron-left"></i>  </button>' +
		'<table  class="table table-striped" id="tabla" >' +
		'<thead>' +
		'<tr>' +
		'	<th scope="col" width="10%"> Código</th>' +
		'	<th scope="col" width="80%"> Nombre</th>' +	
		'	<th scope="col"> </th>' +	
		'	<th scope="col" width="10%"> </th>' +
		'	<th scope="col" width="10%"> </th>' +
		'</tr>' +
		'</thead>' +
		'<tbody id="tablaBody"></tbody>' +
		'</table>');

	for (var i = 0; i < arreglo.length; i++) {
		var id_producto = arreglo[i]['id'];
		var nombre = arreglo[i]['nombre'];			
		var codigo = arreglo[i]['codigo_barra'];
		//BODY DE LA TABLA AGREGAR PRODUCTOS;
		$("#tablaBody").append('<tr>' +
			'<td>' + codigo + '</td>' +
			'<td>' + nombre + '</td>' +		
			'<td id="' + parseFloat(i + 1) + '" ></td>' +		
			'<td>' +
			'<button id=' + codigo + ' class="btn btn-mini" data-toggle="tooltip" data-placement="top" title="Agregar" onclick="productosPorNombre(event,this)"> <i class="fa fa-plus" aria-hidden="true"></i></button>' +
			'</td>' +
			'</tr>');
	}
	$('[data-toggle="tooltip"]').tooltip();
}

//funcion para regresar a la tabla resumen
function regresar() {
	$("#tablaProductos").hide(); //tabla donde se buscan los productos
	$("#tablaResumen").show(); //tabla donde se ve el resumen
}

function productosPorNombre(e,btn){
	e.preventDefault();
	$("#tablaProductos").show();
	var buscar=btn.id;
	console.error(buscar);
	// buscar = $("#buscar").val();
	//*-solo numeros.
	if (isNaN(buscar)) {
		document.getElementById('buscar').value = '';
		console.log('error');
		return;
	}
	if (buscar.trim() == "") {
		swal("Precaución", "Es necesario ingresar un nombre", "warning");
		return;
	}

	if (!isNaN(buscar)) {
		var sql = 'SELECT p.id,codigo_barra ,nombre,precio_instalacion,precio_mayorista,precio_venta,sb.stockb1,sb.stockb2 '+
		' FROM PRODUCTOS p INNER JOIN stock_bodegas sb ON sb.id_producto=p.id WHERE codigo_barra= "' + buscar.trim() + '"';
	}
	

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			sql: sql,
			tag: 'array_de_datos'
		},
		beforeSend: function (objeto) {
			$("#salidaTabla").html("<img src='imagenes/ajax-loader.gif'>");
		},
		success: function (data) {
			var arreglo = JSON.parse(data);
			ARRPRODUCTOS=arreglo;
			$("#tablaProductos").hide();
			$("#salidaTabla").html("");			
			let precio_instalacion = document.getElementById('precioInstalacion').value=arreglo[0]['precio_instalacion'];
			let precio_mayorista = document.getElementById('precioMayorista').value=arreglo[0]['precio_mayorista'];
			let precio_venta = document.getElementById('precioVenta').value=arreglo[0]['precio_venta'];
			document.getElementById('buscar').value='';
			document.getElementById('nombreProducto').innerHTML='<h3>' +arreglo[0]['nombre'] + '</h3>';
		if (arreglo.length > 0) {
			if (arreglo[0]['stockb1'] <= 2 && arreglo[0]['stockb2'] <= 2 ) {
				console.error('stock minino');
				swal("Advertencia", "El stock es critico es = " + arreglo[0]['stock'], "warning");
			//	cargarDatostablaProductos(arreglo);
			} else {
			//	cargarDatostablaProductos(arreglo);
				let preciosocultos = document.getElementById('datos-productos').style.opacity = "1";
				document.getElementById('checkM').disabled=false;
				document.getElementById('checkI').disabled=false;
				document.getElementById('checkV').disabled=false;
				document.getElementById('stockb1').value=arreglo[0]['stockb1'];
				document.getElementById('stockb2').value=arreglo[0]['stockb2'];
			}
		}

		},
		error: function (request, status, error) {
			console.error("Error: Could not buscarProductos");
		}

	});

}

function cargarPrecio(index){

if(index==1){
console.error('precio instalacion');
var precio=document.getElementById('precioInstalacion').value;
var EstadoColumna = "<span class='badge badge-success'>Instalación</span>";
cargarDatostablaProductos(precio,EstadoColumna);
document.getElementById('checkI').checked=false;
document.getElementById('checkM').disabled=true;
document.getElementById('checkI').disabled=true;
document.getElementById('checkV').disabled=true;


}else if(index==2){
	var precio=document.getElementById('precioMayorista').value;
	var EstadoColumna = "<span class='badge badge-primary'>Mayorista</span>";
	cargarDatostablaProductos(precio,EstadoColumna);
	document.getElementById('checkM').checked=false;
	document.getElementById('checkM').disabled=true;
	document.getElementById('checkI').disabled=true;
	document.getElementById('checkV').disabled=true;
}
else if(index==3){
	var precio=document.getElementById('precioVenta').value;
	var EstadoColumna = "<span class='badge badge-info'>Venta</span>";
	cargarDatostablaProductos(precio,EstadoColumna);
	document.getElementById('checkV').checked=false;
	document.getElementById('checkM').disabled=true;
	document.getElementById('checkI').disabled=true;
	document.getElementById('checkV').disabled=true;

}



}


function cargarDatostablaProductos(precio,EstadoColumna) {	

	if (ARRPRODUCTOS.length > 0) {
		ITEM++;
		$("#buscarCodigoBarra").val("");
		columnaEditable = '<span onclick="transformarEnEditable(this,'+ITEM+')" style="cursor:pointer;">1</span>';
		//*-valores consecutivos del item
		$("#tablaBodyVentas").append('<tr id="fila' + ITEM + '" class="new">' +

			'<td>' + ITEM + '</td>' +
			'<td>' + ARRPRODUCTOS[0]['codigo_barra'] + '</td>' +
			'<td>' + ARRPRODUCTOS[0]['nombre'] + '</td>' +
			'<td>' + columnaEditable + '</td>' +
			'<td id="precio' + ITEM + '">' + precio + '</td>' +
			'<td>' +precio + '</td>' +
			'<td> <button class="btn  btn-danger" id="' + ITEM + '" onclick=removerItem(this)><i class="fa fa-trash" aria-hidden="true"></i></button> </td>' +
			'<td style="display:none;"></td>' +
			'<td style="display:none;">' + ARRPRODUCTOS[0]['codigo_barra'] + '</td>' +
			'<td>'+EstadoColumna+'</td>' +
			'</tr>');


		$("#Filaticket").append('<tr id="fila' + ITEM + '" class="new">' +
			'<td id="colsNombre'+ITEM+'" colspan="3">' + ARRPRODUCTOS[0]['nombre'] + '</td>' +
			'</tr>' +
			'<tr>' +
			'<td></td>' +
			'<td id="colsCantidad'+ITEM+'">1</td>' +
			'<td id="colsPrecio'+ITEM+'">' + ARRPRODUCTOS[0]['precio_venta'] + '</td>' +
			'</tr>'
		);
		calcularValores();
	} else {
		swal("Advertencia", "El producto no existe", "warning");

	}

}

//funcion que calular el valor total de la venta
function calcularValores() {
	
	var columnaValorTotal = 5; //definimos la variable de la columna con el valor total

	var tabla = document.getElementById("tablaBodyVentas"),
		rIndex;
	var nFilas = $("#tablaBodyVentas > tr").length;
	var sumaTotal = 0;
	var montos, montoTrasnformado;

	$("#montoEfectivo").val("");
	$("#vuelto").val("");

	for (var i = 0; i < nFilas; i++) {
		montos = tabla.rows[i].cells[columnaValorTotal].innerHTML;
		montoTrasnformado = convertirNumeros(montos)
		sumaTotal += parseInt(montoTrasnformado);

	}

	var totalSacadoMostrar = Intl.NumberFormat('es-MX').format(redondeo(sumaTotal, 0));

	$("#total").val(totalSacadoMostrar);
	document.getElementById('totalCompra_precio').innerHTML=sumaTotal;
}


function calcularVuelto() {
	var montoEfectivo = $("#montoEfectivo").val();
	var total = $("#total").val();

	$("#vuelto").val(formatearNumeros(convertirNumeros(montoEfectivo) - convertirNumeros(total)));
	console.log("monto efectivo: " + montoEfectivo);
}

/******DESCUENTO**** */
function calcularDescuento(e){
	if (e.keyCode === 13) {
			e.preventDefault();
			var totales = document.getElementById('total').value;
			var descuento = document.getElementById('descuento').value;
			if(!isNaN(descuento)){	
				var desc=(convertirNumeros(totales)) * (descuento/100);
				console.error(desc);
			
			var etotades=convertirNumeros(totales) - desc;
			console.error(formatearNumeros(etotades));
			totales.value=formatearNumeros(etotades);
			$("#total").val(convertirNumeros(totales)-(convertirNumeros(totales)) * (descuento/100));
			}


		}


}
/*------------------------------ Productos  ------------------------------------*/
// RECORRO LA TABLA VENTAS PARA ELIMINAR UNA FILA;
function removerItem(id) {
	//$("#Filaticket").html("");
	var filas = $("#tablaBodyVentas > tr ").length;
	var precioProductoEliminar;
	console.error(filas);
	$("#tablaBodyVentas > tr").each(function () {

		var idRe = id.id;
		if ($("#precio" + idRe).length > 0) { console.log("precio1True"); precioProductoEliminar = $("#precio" + idRe).text(); }
		if ($("#precioPromo" + idRe).length > 0) { console.log("precio2True"); precioProductoEliminar = $("#precioPromo" + idRe).text(); }
		precioProductoEliminar = convertirNumeros(precioProductoEliminar);
		
		console.error("idRe: " + idRe);
		$("#fila" + idRe).remove();

	});
	//*-eliminar la tabla del ticker de impresion
	$("#Filaticket > tr").each(function(){
		var idRe = id.id;
		
		document.getElementById('colsPrecio'+idRe).textContent="";
		document.getElementById('colsCantidad'+idRe).textContent="";
		$("#fila" + idRe).remove();

	});

	if ($("#diferencia").length > 0) {
		console.error("campo diferencia existe aca");
		var diferencia = convertirNumeros($("#diferencia").val());
		diferencia = parseInt(diferencia) + parseInt(precioProductoEliminar);
		console.error("La diferencia es :" + diferencia);
		diferenciaCss(diferencia);
	}

	agregarNumeracionItem();
	calcularValores();
}

//FUNCTION PARA AGREGAR UN ITEM A LA VENTA NUEVA Y RESETEAR CUANDO SE BORRE UN ITEM;
function agregarNumeracionItem() {
	var tabla = document.getElementById("tablaBodyVentas"),
		rIndex;
	var nFilas = $("#tablaBodyVentas > tr").length;
	for (var i = 0; i < nFilas; i++) {
		tabla.rows[i].cells[0].innerHTML = i + 1;

	}

}
/*-------------------------------------------------------------------------------*/

function verificarMedioPago() {
	var selectModoPago = $("#selectModoPago").val();
	console.log("El medio de pago es :" + selectModoPago);
	if (selectModoPago != 1) {
		$("#montoEfectivo").prop('disabled', true);
	} else {
		$("#montoEfectivo").prop('disabled', false);
	}

}
//*-descontar el stock
function descontarStock() {

	var tablaC = document.getElementById("tablaBodyVentas"),
		rIndex;
	var columnaValorTotal = 7;
	var codigos;
	var nFilas = $("#tablaBodyVentas > tr.new").length;


	for (var i = 0; i < nFilas; i++) {
		codigos = tablaC.rows[i].cells[columnaValorTotal].innerHTML;

		var sql = 'UPDATE productos set stock =stock - (1)  WHERE codigo_barra=' + codigos + '';

		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: {
				sql: sql,
				tag: 'crud_productos'
			},

			success: function (data) {
				console.error(data);
				if (data != 1) {
					console.error("No se actualizo el stock " + codigos);
					alert('no actualizo')


				} else {
					console.info("Actualizado el stock " + codigos);
				}
			},
			error: function (request, status, error) {
				console.error("Error: Could not actualizar Total ComprasR");
			}
		});

	}

}


function descontarStockFinal(codigoBarra) {
	var sql = 'UPDATE productos set stock =stock - (1)  WHERE codigo_barra=' + codigoBarra + '';

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			sql: sql,
			tag: 'crud_productos'
		},

		success: function (data) {
			console.error(data);
			if (data == 1) {
				console.info("Actualizado el stock final " + codigoBarra);

			} else {
				console.error("No se actualizo el stock " + codigoBarra);
			}
		},
		error: function (request, status, error) {
			console.error("Error: Could not actualizar stock final");
		}
	});
}

//finalizo venta, descuento stock de los productos
function finalizarVenta(e, nombreDiv) {
	console.error(nombreDiv);
	e.preventDefault();

	var nFilas = $("#tablaBodyVentas > tr").length;

	if (nFilas > 0) {
		var total = $("#total").val();
		var modoPago = $("#selectModoPago").val();

		var sql = 'INSERT INTO ventas ( id_vendedor, medio_pago, fechaVenta,totalVenta,id_turno) ' +
			' VALUES (' + ID_VENDEDOR + ',' + modoPago + ', NOW()  , ' + convertirNumeros(total) + ',' + ID_TURNO + ')';

		console.log('sql ' + sql);

		swal({
			title: "¿Crear nueva venta?",
			text: "¿Esta seguro de crear esta venta?",
			type: "warning",
			confirmButtonColor: "#cc3a1d",
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true,
		},
			function () {
				setTimeout(function () {

					$.ajax({
						type: 'POST',
						url: 'php/consulta.php',
						data: {
							tag: 'insert_return_id',
							sql: sql
						},
						success: function (data) {
							console.log(data);
							if (!isNaN(data)) {						
							document.getElementById('TituloTicket').textContent="TICKET DE VENTAS";	
							document.getElementById('FechaTicket').innerHTML=FECHA +' ' + HORA;		
							document.getElementById('colNombre').innerHTML="Nombre";
							document.getElementById('colCantidad').innerHTML="Cantidad";
							document.getElementById('colPrecio').innerHTML="Precio";
							document.getElementById('totalCompra').innerHTML="Total";
							document.getElementById('leyendaPie').innerHTML="Gracias Por Su Compra www.yoimplemento.cl";
							insertarProductos(data);
								//desactivar ticket por ahora
								//imprimir(nombreDiv);

							} else {
								console.error("No es correto");
							}
						},
						error: function (request, status, error) {
							console.error("Error: Could not finalizar venta");
						}
					});

				}, 10);
			});


	} else {
		swal("Precaución", "No hay productos para vender", "warning");

	}
}

//funcion para insertar pruductos relacionados a la ventas
function insertarProductos(idVenta) {

	var nFilas = $("#tablaBodyVentas > tr").length;
	var contador = 0;
	var porcentaje = 0,
		exito = 0;
	console.log('llego hasta aca');

	$("#tablaBodyVentas > tr").each(function (index, value) {
		console.log("index: " + index);
		var codigoBarras = value.cells[1].innerHTML;
		
		var nombre = value.cells[2].innerHTML; // no  se usa nombre
		var cantidad = value.cells[3].textContent;
		
		var precio = convertirNumeros(value.cells[4].innerHTML);
		var totalPrecio = (precio * cantidad);
		

		var sql = 'insert into ventas_relacional (codigoProducto,precioUnitario,cantidad,totalUnitario,idVenta, nombreProducto) ' +
		' VALUES ("' + codigoBarras + '",' + precio + ','+cantidad+','+totalPrecio+',' + idVenta + ',"'+nombre+'")';
		
		console.log(sql);
		
		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			async: false,
			data: {
				tag: 'crud_productos',
				sql: sql
			},
			success: function (data) {
				contador++;
				exito++;
				console.log(data);
				console.log("contador: " + contador + " y nFilas " + nFilas);
				if (data == 1) { descontarStockFinal(codigoBarras) }
			
					
					swal("Venta creada", "los datos fueron guardados exitosamente", "success");
					setTimeout('location.reload()', 2000);

				
			},
			error: function (request, status, error) {
			
				swal("Venta creada", "los datos fueron guardados", "info");
				setTimeout('location.reload()', 7000);

				console.error("Entro aca");

			}

		});

	});
}

//*-imprimir el ticket

function ocultarTicket(){
let ticket = document.getElementById('conTicket').style.display = "none";
let preciosocultos = document.getElementById('datos-productos').style.opacity = "0.3"; 
$("#tablaProductos").hide();
console.error(FECHA +' ' + HORA);		
	if (TIPO_TURNO == 0) {
		swal('Precaución', 'Debe iniciar turno para ingresar una venta', 'info');
		setTimeout(function () {
			window.location = 'ver_productos.php'
		}, 2000);
	}
}
function imprimir(nombreDiv) {

	var contenido = document.getElementById(nombreDiv).innerHTML;
	var contenidoOriginal = document.body.innerHTML;
	document.body.innerHTML = contenido;
	window.print();
	document.body.innerHTML = contenidoOriginal;

}

