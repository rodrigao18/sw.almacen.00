var editando = false;
//funcion para tranformar una celda editable
/* Indice: 1 -> cantidad de producto
					 2 -> indice para difrencia la funcion para la tercera tabla
					 solo quiero que actualize al hacer enter
					 3 -> precio de producto

	estado: 11-> para la cotizacion
					22-> detalle cotizacion (guarda en bd);
          33-> orden de compra
          44-> detalle orden de compra (guarda en la bd)
*/
function transformarTablaPromedio(nodo,indiceEditable,indiceColumna) {

  //El nodo recibido es SPAN

  if (editando == false) {
    var nodoTd = nodo.parentNode; //Nodo TD
    var nodoTr = nodoTd.parentNode; //Nodo TR
    var nodosEnTr = nodoTr.getElementsByTagName('td');    
        var cantidad = convertirNumeros(nodoTd.textContent);
		var nuevoCodigoHtml = '<td><input type="number" id="cantidad" min=1 value=' + cantidad + ' onkeydown="pulsarPromedio(event, this,9,'+indiceEditable+','+indiceColumna+',2)" width="100%" onkeyup="salirPromedio(event,this,'+cantidad+','+indiceEditable+','+indiceColumna+',2)" onkeypress="pulsarPromedio(event, this,13,'+indiceEditable+','+indiceColumna+',2)"></td>';
        nodoTd.innerHTML = nuevoCodigoHtml;

    editando = "true";
  } else {
    alert('Solo se puede editar una línea. Recargue la página para poder editar otra');
  }


}

//funcion pulsar
/* e: event,nodo: nodo, indice: 1 (cantidad),2 (nombre),3 (precio). estado 0 cotizacion, 1 detalle.
 */
function pulsarPromedio(e, nodo, indice,indiceEditable,indiceColumna) {
  //*-validamos que tecla se pulso si enter o tab
  //*-hace la misma funcion{}
  //*-si es enter
  if (indice == 13) {
    var tecla = 13;
  //*-si es tab  
  } else if (indice == 9) { 
    var tecla = 9;
  }
  if (e.keyCode === tecla && !e.shiftKey) {

    var nodoTd = nodo.parentNode; //Nodo TD
    var nodoTr = nodoTd.parentNode; //Nodo TR
    var nodosEnTr = nodoTr.getElementsByTagName('td');        
      var idVentaR = nodosEnTr[0].textContent;
      var cantidad = document.getElementById('cantidad').value;    
      var nuevoCodigoHtml = '<td> <span class="editar" onclick="transformarTablaPromedio(this,'+indiceEditable+','+indiceColumna+',2)" style="cursor:pointer;"> ' + formatearNumeros(cantidad) + '</span> </td>';   

      nodoTd.innerHTML = nuevoCodigoHtml;

    editando = false;
    
	sumColTabPromedio(1,indiceColumna,2);
   
  }
}

//*-salir de la celda editable si no deseo editar
function salirPromedio(event, nodo,valorAnterior,indiceEditable,indiceColumna) {
	if (event.code === 'Escape' || event.keyCode === 27) {
	  console.log('escape');
		  console.info("valor de nodo :" + nodo);
  
		  var nodoTd = nodo.parentNode; //Nodo TD
	  var nodoTr = nodoTd.parentNode; //Nodo TR
	  var nodosEnTr = nodoTr.getElementsByTagName('td');
	  
		var cantidad = document.getElementById('cantidad').value;
		var nuevoCodigoHtml = '<td> <span class="editar" onclick="transformarTablaPromedio(this,'+indiceEditable+','+indiceColumna+',2)" style="cursor:pointer;"> ' + valorAnterior + '</span> </td>';
  
		nodoTd.innerHTML = nuevoCodigoHtml;
	
	  editando = false;
	}
  }
