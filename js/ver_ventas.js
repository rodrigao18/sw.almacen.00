var VENDEDOR;
var ACTIVO=true;
let cargar_ventaInicio_onchange = (NIVEL,ID_VENDEDORLOGUEADO)=>{
	$("#tablaBody").html("").fadeIn('slow');
	ACTIVO=false;
	if(ACTIVO==true){
	lenguaje();
	return;
	}
	
	var fecha_inicio = $("#fecha_inicio").val();
	var fecha_termino = $("#fecha_termino").val();
	cargarVentas(fecha_termino,fecha_inicio);

}

/*let cargarVentas = async (fecha_actual) => { 
	const baseUrl = 'php/consultaFetch.php';
	let consulta = `SELECT numeroVenta,id_vendedor,DATE(fechaVenta) as fecha,TIME(fechaVenta) as hora ,totalVenta 
	from ventas where fechaVenta=${fecha_actual} ORDER BY numeroVenta DESC`;
	console.error(consulta);
	const sql = {sql: consulta, tag: `array_datos`} 
	
	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();
		//*-se parsea solo la respuesta del Json enviada por el servidor.
		let array = JSON.parse(data);
			
		console.log(array);		
		tablaVentas(array);
		
		//*-promesa de la funcion denguaje la ejecuto a la espera
		//*-de la respuesta del servidor.	
		const botones = await lenguaje();	
	
	} catch (error) {
		console.log('error en la conexion ', error);
	}

}*/
function cargarVentas(fecha_actual,fecha_termino){	

	if (NIVEL > 0) {
		var filtroVendedor = 'AND id_vendedor=' + ID_VENDEDORLOGUEADO;
		var sql=`SELECT numeroVenta,id_vendedor,DATE(fechaVenta) as fecha,TIME(fechaVenta) as hora ,totalVenta 
		from ventas where fechaVenta BETWEEN '${fecha_termino} 00:00:00' AND  '${fecha_actual} 23:58:59'  ${filtroVendedor} ORDER BY numeroVenta DESC`;

	}else{
		var sql=`SELECT numeroVenta,id_vendedor,DATE(fechaVenta) as fecha,TIME(fechaVenta) as hora ,totalVenta 
		from ventas where fechaVenta BETWEEN '${fecha_termino} 00:00:00' AND  '${fecha_actual} 23:58:59' ORDER BY numeroVenta DESC`;
	}

	
	
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			tag: 'array_de_datos',
			sql: sql
		},
		success: function (data) {
			var arreglo = JSON.parse(data);
			
			
			tablaVentas(arreglo);
			
		},
		error: function (request, status, error) {
			console.error("Error: Could not cargarCotizaciones");
		}
	});


}
/*let tablaVentas = (arreglo) => {
	
	
	let tbody = document.getElementById('tablaBody');
		
	for (let i of arreglo) {
		
		console.log(i['id_vendedor']);
		tbody.innerHTML +=
		`<tr>		   
		   <td>${i['numeroVenta']}</td>
		   <td>${VENDEDOR[`${i['id_vendedor']}` - 1 ]["nombreVendedor"]}</td>
		   <td>${i['fecha']}</td>
		   <td>${i['hora']}</td>		  
		   <td>${i['totalVenta']}</td>		  
		   <td><form method="POST" action="detalle_venta.php">
		   <input type="hidden" class="form-control" id="id" name="id" value="1">
		   <input type="hidden" class="form-control" id="numeroVenta" name="numeroVenta" value=${i['numeroVenta']}>
		   <button type="submit" class="btn btn-secondary" data-toggle="tooltip"		 
			data-placement="top" title="Editar" name="id" value=${i['numeroVenta']}><i class="fas fa-edit" aria-hidden="true"></i></button></form></td>
			<td ><button class="btn  btn-danger" data-toggle="tooltip" data-placement="top" title="Borrar" onclick=eliminarProducto(event,${i['numeroVenta']})><i class="fa fa-trash" aria-hidden="true"></i></button></td>			
		 </tr>`
	
	 	
	}


	$('[data-toggle="tooltip"]').tooltip();
	
 }*/

function tablaVentas(arreglo){

	for (var i = 0; i < arreglo.length; i++) {
		$("#tablaBody").append(
			`<tr>		   
			<td>${arreglo[i]['numeroVenta']}</td>
			<td>${VENDEDOR[`${arreglo[i]['id_vendedor']}` - 1 ]["nombreVendedor"]}</td>
			<td>${arreglo[i]['fecha']}</td>
			<td>${arreglo[i]['hora']}</td>		  
			<td>${arreglo[i]['totalVenta']}</td>		  
			<td><form method="POST" action="detalle_venta.php">
			<input type="hidden" class="form-control" id="id" name="id" value="1">
			<input type="hidden" class="form-control" id="numeroVenta" name="numeroVenta" value=${arreglo[i]['numeroVenta']}>
			<button type="submit" class="btn btn-secondary" data-toggle="tooltip"		 
			 data-placement="top" title="Editar" name="id" value=${arreglo[i]['numeroVenta']}><i class="fas fa-edit" aria-hidden="true"></i></button></form></td>
			 <td ><button class="btn  btn-danger" data-toggle="tooltip" data-placement="top" title="Borrar" onclick=eliminarProducto(event,${arreglo[i]['numeroVenta']})><i class="fa fa-trash" aria-hidden="true"></i></button></td>			
		  </tr>`
		);

	}
	$('[data-toggle="tooltip"]').tooltip();
	if(ACTIVO==true){
		lenguaje();
		}
}


 let consultarVendedor = async (fecha_actual,fecha_termino) => {

	
	const baseUrl = 'php/consultaFetch.php';
	let consulta = `SELECT nombreVendedor  
	FROM vendedores `;
	
	const sql = {sql: consulta, tag: `array_datos`} 
	
	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();
		//*-se parsea solo la respuesta del Json enviada por el servidor.
		let array = JSON.parse(data);
		console.log(array);
		VENDEDOR=array;
		cargarVentas(fecha_actual,fecha_termino);
	
		
	} catch (error) {
		console.log('error en la conexion en consultaVendedor ', error);
	}

  }


function lenguaje() {

	var f = new Date();
	var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();

	var table=$('#tablaVentas').DataTable({

		language: {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			"infoFiltered": "(Filtrado de _MAX_ total entradas)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Entradas",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		},
		"aria": {
			"sortAscending": ": activate to sort column ascending",
			"sortDescending": ": activate to sort column descending"
		},
		"order": [[1, "desc"]],
		"stateSave":true
	});


     new $.fn.dataTable.Buttons(table, {
		buttons: [
			{
				extend: 'excelHtml5',
				title: 'ver_ventas' + fecha + ''
            }, {
				extend: 'pdfHtml5',
				title: 'ver_ventas' + fecha + ''
            }]

	});

	table.buttons(0, null).container().prependTo(
		table.table().container()
	);


}

function eliminarProducto(e, id) {
	e.preventDefault();

	$.ajax({
		type: 'POST',
		url: 'php/funcionesProductos.php',
		data: {
			tag: 'eliminarProducto',
			id: id
		},
		success: function (data) {
			console.log(data);
			if (data == 1) {
				alert('Borrado exitoso');
				cargarVentas(e);
			} else {
				alert('No Borrado');
			}
		},
		error: function (request, status, error) {
			console.log(request);
			console.log(status);
			console.log(error);
			alert("Error: Could not delete");
		}
	});

}


