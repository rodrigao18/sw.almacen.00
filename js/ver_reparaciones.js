
function cargarTecnicosSelect(){
	
	var sql = 'SELECT id,nombre FROM tecnicos';
	//AJAX	
	$.ajax({
	type: 'POST',
	url:  'php/consulta.php', 
	data: {sql: sql,tag: 'tecnicos'},
			
	success:function (data){
				$('#select_tecnicos').html(data).fadeIn();
				$('#select_estado option[value="0"]').attr("selected", true); 
				lenguaje(); 
				filtroSelects();
	},
	error: function (request, status, error)
				{alert('Error: Could not categoria');
	}
	})

}

function filtroSelects(){

	let tbody = document.getElementById('tablaBody');
	tbody.innerHTML='';

	var idTecnico = document.getElementById("select_tecnicos").value;
	var idTipo = document.getElementById("select_estado").value;

	var filtroTecnico= 'WHERE id_tecnico=' + idTecnico;
	var filtrotipo= 'AND estado= '+ idTipo + ' ';
	
	if(idTecnico == 0) { filtroTecnico = ''; }
	if(idTipo == 0)	  { filtrotipo = ''; }
	
	const consulta=`SELECT r.id, DATE(r.fecha) as fecha,c.nombre,marca,modelo,falla,r.estado 
	FROM reparaciones r INNER JOIN reparaciones_relacional rr ON rr.id_reparacion=r.id JOIN clientes c ON c.id=r.id_cliente ${filtroTecnico} ${filtrotipo}`;
	

	cargarReparaciones(consulta);


}

//*-cargar datos mediante async wait()
let cargarReparaciones = async (consulta) => { 
	
	const baseUrl = 'php/consultaFetch.php';
    /*let consulta=`SELECT r.id, DATE(r.fecha) as fecha,c.nombre,marca,modelo,falla,r.estado 
    FROM reparaciones r INNER JOIN reparaciones_relacional rr ON rr.id_reparacion=r.id JOIN clientes c ON c.id=r.id_cliente`;*/
	
	const sql = {sql: consulta, tag: `array_datos`} 
	
	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();
		//*-se parsea solo la respuesta del Json enviada por el servidor.
		let array = JSON.parse(data);		
		//const botones = await lenguaje();		
		tablaReparaciones(array);		
		//*-promesa de la funcion denguaje la ejecuto a la espera
		//*-de la respuesta del servidor.	
	
		
	} catch (error) {
		console.log('error en la conexion ', error);
	}	
}

//*-productos
let tablaReparaciones = (arreglo) => {
	
	let tbody = document.getElementById('tablaBody');
	
	var EstadoColumna; 
	for (let i of arreglo) {
		console.error('');
		if(i['estado']==1){
			EstadoColumna = "<span class='badge badge-info'>En Taller</span>";
		}else if(i['estado']==2){
			EstadoColumna = "<span class='badge badge-dark'>Terminado</span>";
		}else if(i['estado']==3){
			EstadoColumna = "<span class='badge badge-success'>Entregado</span>";
		}
		
		tbody.innerHTML +=
		`<tr>		   
		   <td>${i['id']}</td>
		   <td>${i['fecha']}</td>
		   <td>${i['nombre']}</td>
		   <td>${i['marca']}</td>
           <td>${i['modelo']}</td>		  
		   <td>${i['falla']}</td>
		   <td>${EstadoColumna}</td>			
		   <td><form method="POST" action="editar_reparacion.php">
		   <button type="submit" class="btn btn-secondary" data-toggle="tooltip"
			data-placement="top" title="Editar" name="id" value=${i['id']}><i class="fas fa-edit" aria-hidden="true"></i></button></form></td>			
			<td ><button class="btn  btn-danger" data-toggle="tooltip" data-placement="top" title="Borrar" onclick=eliminarProducto(event,${i['id']})><i class="fa fa-trash" aria-hidden="true"></i></button></td>			
		 </tr>`
	 	
	}
	
	$('[data-toggle="tooltip"]').tooltip();
	
 }


function lenguaje() {

	var f = new Date();
	var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();

	var table=$('#tablaProductos').DataTable({

		language: {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			"infoFiltered": "(Filtrado de _MAX_ total entradas)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Entradas",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		},
		"aria": {
			"sortAscending": ": activate to sort column ascending",
			"sortDescending": ": activate to sort column descending"
		},
		"order": [[1, "desc"]],
		"stateSave":true,
		"lengthMenu":[ 100, 125, 150, 175, 1000 ]
	});


     new $.fn.dataTable.Buttons(table, {
		buttons: [
			{
				extend: 'excelHtml5',
				title: 'ver_ventas' + fecha + ''
            }, {
				extend: 'pdfHtml5',
				title: 'ver_ventas' + fecha + ''
            }]

	});

	table.buttons(0, null).container().prependTo(
		table.table().container()
	);


}

window.onload = cargarTecnicosSelect