//CARGAR CATEGORIA
 function cargar_categoria() {

	var sql = 'SELECT id,nombre_categoria FROM categoria';
	//AJAX	
	$.ajax({
	type: 'POST',
	url:  'php/consulta.php', 
	data: {sql: sql,tag: 'categoria'},
			
	success:function (data){
				$('#select_categoria').html(data).fadeIn();
			//	$('#select_categoria option[value="1"]').attr("selected", true);  
				cargar_bodega();
	},
	error: function (request, status, error)
				{alert('Error: Could not categoria');
	}
	})

}
window.onload = cargar_categoria
/*-----------------------------------------------*/
function cargar_bodega() {

	var sql = 'SELECT id,nombre FROM bodega';
	//AJAX	
	$.ajax({
	type: 'POST',
	url:  'php/consulta.php', 
	data: {sql: sql,tag: 'bodegas'},
			
	success:function (data){
				$('#select_bodega1').html(data).fadeIn();
				$('#select_bodega2').html(data).fadeIn();
				$('#select_bodega1 option[value="1"]').attr("selected", true);
				$('#select_bodega2 option[value="2"]').attr("selected", true);    
	},
	error: function (request, status, error)
				{alert('Error: Could not categoria');
	}
	})
}



/*----------------------------Agregar productos------------------------*/

function comprobarProducto(){
	if($("#codigoProducto").val() != ""	){
		var codigo_producto = $("#codigoProducto").val();
		console.error(codigo_producto);

		var sql = 'SELECT count(*) FROM productos where codigo_barra=' + codigo_producto;
	
		//-*AJAX	
		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			data: { sql: sql, tag: 'array_de_datos' },

			success: function (data) {
				var arreglo = JSON.parse(data);
				existe = arreglo[0][0];
				if (existe < 1) {
			
					} else {
						$.notify({
							title: "Codigo existente : ",
							message: "El Codigo de estes producto ya existe en la base de datos:",
							icon: 'fas fa-exclamation-circle'
						}, {
							type: "danger",
							placement: {
								from: "top",
								align: "right"
							},
							offset: 70,
							spacing: 70,
							z_index: 1031,
							delay: 1000,
							timer: 1000
						});
					document.getElementById('codigoProducto').focus();					
					}
				
			},
			error: function (request, status, error) {
				console.error("Error: Could not comprobarCLiente");
			}
		});

	}else{
		var codigo_producto = $("#codigoProducto").val();
		console.error(codigo_producto);		
	}	
	return;
}

//*-guardar productos;	
function GuardarProducto(e) {
	e.preventDefault();
	var nombre = $("#nombreProducto").val();
	var codigoProducto = $("#codigoProducto").val();	
	var idCategoria = document.getElementById("select_categoria").value;
	
	var precioInstalacion = $("#precioInstalacion").val();
	var precioMayorista = $("#precioMayorista").val();
	var precioVenta = document.getElementById('precioVenta').value;

	 if(idCategoria==0){
		swal("Faltan datos", "Seleccione una bodega o categoria", "info");
		return;	 
	 }

	var sql = 'insert into productos (codigo_barra,nombre, precio_instalacion, precio_mayorista,precio_venta,id_categoria)' +
		'VALUES("' + codigoProducto + '","' + nombre + '",' + precioInstalacion + ',' + precioMayorista + ', '+
		' '  + precioVenta + ',' + idCategoria + ')';

	 console.error(sql);
	
	if ($("#nombreProducto").val() == "" || $("#stock1").val() == "" ||
		$("#precioInstalacion").val() == 0 || $("#precioInstalacion").val() == "" ||
		$("#precioVenta").val() == 0 || $("#precioVenta").val() == "") {
		swal("Debe Llenar los Campos!", "", "info");
	

	} else{
		
		$.ajax({
			type: 'POST',
			url: 'php/consulta.php',
			async: true,
			data: {
				tag: 'insert_return_id',
				sql: sql
			},
			success: function (data) {
				
				if (!isNaN(data)) {
					productoRelacional(data);				
				} else {
					swal("Error", 'Revice el nombre del producto las pulgadas deber ser por ejemplo (14"") ', "error");	 
				}
			},
			error: function (request, status, error) {
				console.error("Error: Could not  guardarProducto");
			}
		});
	}	
}

function productoRelacional(idProducto){

	
	var stock1 = document.getElementById('stock1').value;
	var stock2 = document.getElementById('stock2').value;

	var sql = 'insert into stock_bodegas (stockb1,stockb2,id_producto) ' +
	' VALUES (' + stock1 + ',' + stock2 + ',' + idProducto + ')';
	
	console.log(sql);
	
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		async: false,
		data: {
			tag: 'crud_productos',
			sql: sql
		},
		success: function (data) {		
			swal("Insert!", "El producto fue ingresado correctamente!", "success");	
			
		},
		error: function (request, status, error) {
			console.error("Entro aca");
		}

	});



}


