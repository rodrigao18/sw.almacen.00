//*-VARIABLES GLOBALES;
var FECHA;
var FECHA_ATRAS;
var FECHA_PROXIMA;
var ARRAYCATEGORIAS = [];

function cargarTablasAtras() {
	document.getElementById('btn_adelante').disabled = false;	
	if (FECHA_ATRAS == 2018) {
		document.getElementById('btn_atras').disabled = true;	
	 }
   comprobarTabladetallesGenerales(FECHA_ATRAS);
}
function cargarTablasProxima() {
	document.getElementById('btn_atras').disabled = false;	
	if (FECHA_PROXIMA == 2030) {
		document.getElementById('btn_adelante').disabled = true;	
	 }
   comprobarTabladetallesGenerales(FECHA_PROXIMA);
}
/****** TABLA DETALLES GASTOS GENERALES ******* */
//*-cargar categorias
function cargarCategorias(comprobar) {
  
	var arregloCategorias = [];
  var sql = 'SELECT nombre_categoria FROM categorias order By id Asc ';
  
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			tag: 'array_de_datos',
			sql: sql
		},
		success: function (data) {
			var arreglo = JSON.parse(data);
			for (var i = 0; i < arreglo.length; i++) { 
				arregloCategorias.push(arreglo[i]['nombre_categoria'])

      }
      ARRAYCATEGORIAS = arregloCategorias;
      tablaDetallesGastGen(arreglo,comprobar,arregloCategorias);
		},
		error: function (request, status, error) {
			console.error("Error: Could not a-ctualizarMontos");

		}
	});

}



//-*segunda tabla detalles gastos genetales
function tablaDetallesGastGen(arreglo,comprobar,arregloCategorias) {
 $("#tablaBodyDetalles").html('');
  var tbody = 'tablaBodyDetalles';
  var cantidad = 0;
  console.error(arreglo);
  cols = 13;
  var tablaC = document.getElementById(tbody),
  rIndex; 
  var nFilas = $('#' + tbody + ' > tr.operativos').length;
  for (var i = arreglo.length-1; i >= 0; i--) {
    $("#tablaBodyDetalles").prepend(
      '	<tr class="operativos"  id="fila' + (i + 1) + '">' +	
      '	    <td class="encabezado-tabla-detalles">' + arreglo[i]['nombre_categoria'] + '</td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,1)">0</span></td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,2)">0</span></td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,3)">0</span></td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,4)">0</span></td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,5)">0</span></td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,6)">0</span></td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,7)">0</span></td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,8)">0</span></td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,9)">0</span></td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,10)">0</span></td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,11)">0</span></td>' +
      '	    <td><span onclick="transformarEnEditable(this,2,12)">0</span></td>' +      
      '	    <td id="totalItemGeneral'+(i+1)+'">0</td>' +       
      '</tr>'
    );
  }  
 
  if (comprobar == 1) {   
  	setTabla(2);    

  
  } 
  
}

function matrizDeGastosGenerales(arregloCategorias) {
  var matriz = [];
  var arrayTemp = [];
  var tbody = 'tablaBodyDetalles';
  var cantidad = 0;
  var tablaC = document.getElementById(tbody),
  rIndex; 
  var nFilas = $('#' + tbody + ' > tr.operativos').length;
//  console.log(tablaC.rows[0].cells[1].innerText);
//  console.log(tablaC.rows[1].cells[1].innerText);

  for (var columna = 1; columna <= 12; columna++) {
    for (var fila = 0; fila < nFilas; fila++) { 
      cantidad = tablaC.rows[fila].cells[columna].innerText;
      var cantTras = convertirNumeros(cantidad)
      arrayTemp.push(cantTras);

    }
   
    matriz.push(arrayTemp);
    arrayTemp = [];

  }  
  graficoGastosgenerales(matriz,arregloCategorias);
 }
/*************************-FIN GASTOS GENERALES-******************************/


//*-funcion para sumar las columnas hacia abajo
function sumarColumnas(index, indiceColumna) {
  //*-indiceColumna : se suma sola la columa seleccionada;
  var suma=0;  
  var cantidad; 
  var contador = 0;
  
  if (index == 1) {    
  var  tbody = 'tbodyResultOper';
  } else { 
  var  tbody = 'tablaBodyDetalles';
  }
  var nFilas = $('#'+tbody+' > tr.operativos').length;
  
  var tablaC = document.getElementById(tbody),
  rIndex; 


  for (var columna = indiceColumna; columna <= indiceColumna; columna++) {
    
    for (var i = 0; i <= nFilas-1	; i++) {
      
      cantidad = tablaC.rows[i].cells[columna].innerText;
      var cantTrans = convertirNumeros(cantidad);
      //*-sumar los montos    
      suma += parseInt(cantTrans); 
	  //*-actualizo los montos

     	 updateMontos((i + 1), columna, cantTrans, contador,index);
      
    }
    if (index == 1) {
      
      document.getElementById('totalAcumulado' + columna).innerHTML = formatearNumeros(suma);

      //*-notity
      $.notify({
        title: "Monto modificado : ",
        message: "Se actualizó la información de la tabla",
        icon: 'fas fa-check'
      }, {
          type: "info",
          placement: {
            from: "top",
            align: "right"
          },
          offset: 70,
          spacing: 70,
          z_index: 1031,
          delay: 1000,
          timer: 2000
        });
     //*--------- 
    } else {

       //*-notity
       $.notify({
        title: "Monto modificado : ",
        message: "Se actualizó la información de la tabla",
        icon: 'fas fa-check'
      }, {
          type: "info",
          placement: {
            from: "top",
            align: "right"
          },
          offset: 70,
          spacing: 70,
          z_index: 1031,
          delay: 1000,
          timer: 2000
        });
     //*--------- 
      document.getElementById('totalAcumuladoDetalle' + columna).innerHTML = formatearNumeros(suma);
      
    }   
    suma = 0;
  } 
  matrizDeGastosGenerales(ARRAYCATEGORIAS);
  sumarFilas(index);
}


//*-compruebo las tablas han sido creadas
//*-si no las creo.
/*function comprobarCreacionTablas(fecha) {
  FECHA = fecha;
  FECHA_ATRAS = (fecha) - 1;
  FECHA_PROXIMA = parseInt(fecha) + 1

  document.getElementById('año').innerHTML = fecha;
  var sql = 'SELECT COUNT(*) FROM resultado_operacional where año='+FECHA;
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		async: false,
		data: {
			tag: 'array_de_datos',
			sql: sql
		},
		success: function (data) {	
      var arreglo = JSON.parse(data);
      if (arreglo[0][0] > 1) {
        console.log('existe');       
      comprobarTabladetallesGenerales();
	
      } else {
      //  runTableSave(1);
      }	
		},
		error: function (request, status, error) {		
			console.error("Entro insert into");
		}

	});
}*/
//*-funcion que comprueba si existen datos
//*-en la tabla detalles generales
function comprobarTabladetallesGenerales(fecha) {

  FECHA = fecha;
  FECHA_ATRAS = (fecha) - 1;
  FECHA_PROXIMA = parseInt(fecha) + 1

  document.getElementById('año').innerHTML = fecha;

  var sql = 'SELECT COUNT(*) FROM detalle_gastos_generales where año='+FECHA;
  console.log(sql);
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		async: false,
		data: {
			tag: 'array_de_datos',
			sql: sql
		},
		success: function (data) {	
      var arreglo = JSON.parse(data);
      if (arreglo[0][0] > 1) {
        console.log('existe');        
        setTimeout(function(){cargarCategorias(1); }, 500);
      } else {        
      
      }
		
		},
		error: function (request, status, error) {		
			console.error("Entro insert into");
		}

	});


}

//*-recorreo la tabla para extraer los datos
//*-y llamar a la funcion para guardar
function runTableSave(indice) {
 
  if (indice == 1) {    
    var tbody = 'tbodyResultOper';
    var cols = 7; 
    } else{ 
    var tbody = 'tablaBodyDetalles';    
    var cols = 13; 
    }
    var nFilas = $('#'+tbody+' > tr.operativos').length;
    
    var tablaC = document.getElementById(tbody),
    rIndex; 
  
  for (var columna = 1; columna < cols; columna++) {
   
    for (var i = 0; i < nFilas; i++) {	
       
		   cantidad = tablaC.rows[i].cells[columna].innerText;
       var cantTrans = convertirNumeros(cantidad);      
      // insertMontos((i+1),2030,columna,cantTrans,indice);
		 }

	   } 
}
function insertar() {

  for (var columna = 1; columna < 13; columna++) {
   
    for (var i = 0; i < 26; i++) {     
     
      var sql = 'INSERT INTO detalle_gastos_generales(mes,año,categoria,monto)VALUES(' + columna + ',2025,' + (i + 1) + ',0)';
      console.log(sql);
      $.ajax({
        type: 'POST',
        url: 'php/consulta.php',
        async: false,
        data: {
          tag: 'crud_productos',
          sql: sql
        },
        success: function (data) {
    
        
        },
        error: function (request, status, error) {
          console.error("Entro insert into");
        }
      });


    }


  }
}
/****CRUD TABLA****/
//funcion ingresar montos vacios si no hay datos
//*-en la tabla resultados_operacional y detalles generales;
function insertMontos(mes,fecha,categoria,cantidad,indice){

  if (indice == 1) {
    var sql = 'INSERT INTO resultado_operacional(mes,año,categoria,monto)VALUES(' + mes + ',' + fecha + ',' + categoria + ',' + cantidad + ')';
  } else { 
    var sql = 'INSERT INTO detalle_gastos_generales(mes,año,categoria,monto)VALUES('+categoria+','+fecha+','+mes+','+cantidad+')';	
  }

	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		async: false,
		data: {
			tag: 'crud_productos',
			sql: sql
		},
		success: function (data) {	

		
		},
		error: function (request, status, error) {		
			console.error("Entro insert into");
    }
  });
  
}
//*-funcion que hace un update de los montos
//*-ingresados en cada celda.
function updateMontos(mes, categoria, monto, contador, index) { 
console.log('fecha' + FECHA);
  if (index == 1) {
    var sql = 'UPDATE resultado_operacional set monto=' + monto + ' where mes=' + mes + ' and categoria=' + categoria + ' and año='+FECHA+'';
  } else { 
    var sql = 'UPDATE detalle_gastos_generales set monto=' + monto + ' where mes=' + categoria + ' and categoria=' + mes + ' and año='+FECHA+'';
  }
  console.log(sql);
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		async: false,
		data: {
			tag: 'crud_productos',
			sql: sql
    },  
    success: function (data) {
      contador++;
         
		},
		error: function (request, status, error) {		
			console.error("Entro insert into");

		}

	});
}
//*-setear tabla resultado_operacional y detalle general
function setTabla(indice) {
 
    var tbody = 'tablaBodyDetalles';    
    var cols = 13;     
    var nFilas = $('#'+tbody+' > tr.operativos').length;
    console.info('nFilas ' + nFilas);
    var tablaC = document.getElementById(tbody),
    rIndex; 
 
 
    var sql = 'SELECT monto FROM detalle_gastos_generales where año=' + FECHA;
	

  console.log(sql);
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		async: false,
		data: {
			tag: 'array_de_datos',
			sql: sql
		},
		success: function (data) {	
        var arreglo = JSON.parse(data);
      
         var contador = 0; 
         
		for (var columna = 1; columna < cols; columna++) {
         
         for (var i = 0; i < nFilas; i++) {
          
          
			      tablaC.rows[i].cells[columna].innerHTML = "<span onclick='transformarEnEditable(this,2," + (columna) + ")'>" + formatearNumeros(arreglo[contador]['monto']) + "</span>";             

            contador++;           
            
      }
      
      }
      sumarValoresSeteados(indice);
		},
		error: function (request, status, error) {		
			console.error("Entro insert into");
		}	
	});
	
}

//*-sumar valores seteados
function sumarValoresSeteados(indice) {
  if (indice == 1) {    
    var tbody = 'tbodyResultOper';
    var cols = 7; 
    } else{ 
    var tbody = 'tablaBodyDetalles';    
    var cols = 13; 
    }
    var nFilas = $('#'+tbody+' > tr.operativos').length;
    
    var tablaC = document.getElementById(tbody),
    rIndex; 
  var suma = 0;
  var cantidad = 0;
  
  for (var columna = 1; columna <= cols; columna++) {
    
    for (var i = 0; i <= nFilas-1; i++) {
      
      cantidad = tablaC.rows[i].cells[columna].innerText;
      var cantTrans = convertirNumeros(cantidad);

      //*-sumar los montos    
      suma += parseInt(cantTrans);      
    }  
    if (indice == 1) {
      document.getElementById('totalAcumulado' + columna).innerHTML = formatearNumeros(suma);
    } else { 
      document.getElementById('totalAcumuladoDetalle' + columna).innerHTML = formatearNumeros(suma);
    }            
      suma = 0;
  }
  sumarFilasSeteados(indice);
}

//*-sumar total egresos seteados.
function sumarFilasSeteados(indice) {

  if (indice == 1) {    
    var tbody = 'tbodyResultOper';
    var cols = 7; 
    } else{ 
    var tbody = 'tablaBodyDetalles';    
    var cols = 13; 
    }
    var nFilas = $('#'+tbody+' > tr.operativos').length;    
    var tablaC = document.getElementById(tbody),
    rIndex; 
    var suma=0;  
    var cantidad;
 
  //recorro las filas hacia abajo   
  for (var fila = 0; fila < nFilas; fila++) {
    //entro a cada celda de la fila recorrida en el for de arriba
     for (var i = 1; i < cols; i++) {
  
       cantidad = tablaC.rows[fila].cells[i].innerText;
       var cantTrans = convertirNumeros(cantidad)      
       suma += parseInt(cantTrans);      
     }
    if (indice == 1) {
      document.getElementById('totalEgresosFila' + (fila + 1)).innerHTML = formatearNumeros(suma);
      document.getElementById('resultOperacionalFila' + (fila + 1)).innerHTML = formatearNumeros(suma);
	
    } else { 
      document.getElementById('totalItemGeneral' + (fila + 1)).innerHTML = formatearNumeros(suma);
      sumaTotalItem();
    }   
     suma = 0;   
   } 
  
}

/**FIN CRUD **/
//*-funcion para sumar laas filas hacia la derecha
function sumarFilas(index) {
  var filaValores = 1;
  var suma=0;  
  var cantidad;
  if (index == 1) {    
  var  tbody = 'tbodyResultOper';
    var cols = 7;  
  } else { 
    var tbody = 'tablaBodyDetalles';
    var cols = 12; 
  }
  var nFilas = $('#'+tbody+' > tr.operativos').length;

  var tablaC = document.getElementById(tbody),
    rIndex;   
  //recorro las filas hacia abajo   
  for (var fila = 0; fila <= nFilas-1; fila++) {
   //entro a cada celda de la fila recorrida en el for de arriba
    for (var i = 1; i <= cols; i++) {
 
      cantidad = tablaC.rows[fila].cells[i].innerText;
      var cantTrans = convertirNumeros(cantidad)      
      suma += parseInt(cantTrans);      
    }
    if (index == 1) {
      document.getElementById('totalEgresosFila' + (fila + 1)).innerHTML = formatearNumeros(suma);
      document.getElementById('resultOperacionalFila' + (fila + 1)).innerHTML = formatearNumeros(suma);
    } else {
      
      document.getElementById('totalItemGeneral'+(fila + 1)).innerHTML = formatearNumeros(suma);
		sumaTotalItem();	  
    }
    suma = 0;   
  }
  if (index == 1) { 
    sumarTotalEgresos();  
  }

}
/******TOTAL ITEMS TABLA DETALLES GASTOS GENERALES */
//sumar total item
function sumaTotalItem() { 
 
  var diferencia = 0;
   var suma=0;  
   var cantidad;
   var tablaC = document.getElementById("tablaBodyDetalles"),
    rIndex;
  var nFilas = $('#tablaBodyDetalles > tr.operativos').length;
 
     for (var columna = 13; columna < 14; columna++) {
      
       for (var i = 0; i < nFilas; i++) {
   
         cantidad = tablaC.rows[i].cells[columna].innerText;
         var cantTrans = convertirNumeros(cantidad)
       
         suma += parseInt(cantTrans); 
        
       }
       
      document.getElementById('totalAcumuladoDetalle'+ columna).innerHTML = formatearNumeros(suma); 
       suma = 0;
      }  
      matrizDeGastosGenerales(ARRAYCATEGORIAS);  
	 
}
 /************* */

//sumar la columna egresos
function sumarTotalEgresos(set) { 
	console.error('set ' + set);
 var diferencia = 0;
  var suma=0;  
  var cantidad;
  var tablaC = document.getElementById("tbodyResultOper"),
    rIndex;  
    for (var columna = 7; columna < 9; columna++) {
     
      for (var i = 0; i < 11; i++) {
  
        cantidad = tablaC.rows[i].cells[columna].innerText;
        var cantTrans = convertirNumeros(cantidad)
      
        suma += parseInt(cantTrans); 
       
      }
     
	  document.getElementById('totalAcumulado' + columna).innerHTML = formatearNumeros(suma);
	  diferencia = document.getElementById('totalAcumulado' + columna).innerHTML = formatearNumeros(suma);	  
	  suma = 0;
	  

	 }
  //*sumar total de item tabla 2
  if (set != 1) {
    sumaTotalItem();
    diferenciaTotalEgresoIngreso();  
   }  
}


//*-suma columa Ingresos
function sumarTotalIngresos() {  
	var suma=0;  
	var cantidad;	
	var tablaC = document.getElementById("tbodyResultOper"),
	  rIndex;
	  //recorre hacia abajo primero la columna 7   
	  for (var columna =7; columna < 9; columna++) {		
		
		for (var i = 0; i < 2; i++) {			
		 
		  cantidad = tablaC.rows[i].cells[columna].innerText;
		  var cantTrans = convertirNumeros(cantidad)      
		  suma += parseInt(cantTrans); 
		 
		}		
		document.getElementById('totalAcumulado' + columna).innerHTML = formatearNumeros(suma);	
      suma = 0;      
	   }       
  diferenciaTotalEgresoIngreso();
}
  
//*-calcular la diferencia entre totalEgreso e ingresos
function diferenciaTotalEgresoIngreso() {
  var suma=0;  
	var cantidad;	
	var tablaC = document.getElementById("tbodyResultOper"),
    rIndex;
    for (var i = 0; i < 12; i++) {
        for (var columna = 7; columna < 9; columna++) {
   
          cantidad = tablaC.rows[i].cells[columna].innerText;
          var cantTrans = convertirNumeros(cantidad);
          console.error(suma=cantTrans - suma);
      }
      //*-agragra un calse css cambia de color si el valor es negativo o positivo;
      if (suma > 0) {
        var resultOp = document.getElementById('resultOperacionalFila' + (i + 1));
        resultOp.className = "resultOperacionalFilaPos";
      } else { 
        var resultOp = document.getElementById('resultOperacionalFila' + (i + 1));
        resultOp.className = "resultOperacionalFila";
      }
      document.getElementById('resultOperacionalFila' + (i + 1)).innerHTML = formatearNumeros(suma);
          suma = 0;
   }
  resultadoOperacional();
  
}


//sumar la columna resultadoOperacional
function resultadoOperacional() { 
  var suma=0;  
  var cantidad;
  var tablaC = document.getElementById("tbodyResultOper"),
    rIndex;  
    for (var columna =9; columna < 10; columna++) {
      
      for (var i = 0; i < 11; i++) {
  
        cantidad = tablaC.rows[i].cells[columna].innerText;
        var cantTrans = convertirNumeros(cantidad)      
        suma += parseInt(cantTrans); 
       
      }
      document.getElementById('totalAcumulado' + columna).innerHTML = formatearNumeros(suma);
      suma = 0;
     }  
     calcularSaldos();
}


//*-calcular los saldos inicial-final 
function calcularSaldos() {
  
  var saldoFinal = 0;
  var saldoInicial = document.getElementById('saldoInicial').value;
  var saldoReal = document.getElementById('saldoReal').value;
  var totalResulOperacional = document.getElementById('totalAcumulado9').innerText; 
  var totalResulOperacionalConv = convertirNumeros(totalResulOperacional);

//*-calculo saldo final
  saldoFinal = parseInt(saldoInicial) + parseInt(totalResulOperacionalConv);
  document.getElementById('saldoFinal').value = formatearNumeros(saldoFinal);
 
//*-calculo saldo efectivo
  var valorEfectivo = 0;
  var saldoRealEfectivo = document.getElementById('saldoReal').value;
  var saldoFinalEfectivo = document.getElementById('saldoFinal').value;
  var salFinalEfecConv = convertirNumeros(saldoFinalEfectivo);
 
 valorEfectivo = parseInt(saldoRealEfectivo) - parseInt(salFinalEfecConv);

  document.getElementById('saldoEfectivo').value = formatearNumeros(valorEfectivo);
}