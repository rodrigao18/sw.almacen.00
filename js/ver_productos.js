var CATEGORIA;



function cargarCategoria(e) {
	
	e.preventDefault();
	var sql ='SELECT id,nombre_categoria FROM categoria ';
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {sql:sql, tag: 'array_de_datos' },
		success: function (data) {
			var arreglo = JSON.parse(data);

			var arr = new Array();

			for (var i = 0; i < arreglo.length; i++) {
				arr[arreglo[i][0].toString()] = arreglo[i][1];

			}
			CATEGORIA = arr;
			cargarProductos();

		},
		error: function (request, status, error) {
			alert("Error: Could not cargarProveedores");
		}
	});
}



//*-cargar datos mediante async wait()
let cargarProductos = async () => { 
	const baseUrl = 'php/consultaFetch.php';
	let consulta=`SELECT id, codigo_barra, nombre,precio_instalacion,precio_mayorista,precio_venta,id_categoria FROM productos ORDER BY codigo_barra`;
	
	const sql = {sql: consulta, tag: `array_datos`} 
	
	try {
		//*-llamar ajax al servidor mediate api fetch.
		const response = await fetch(baseUrl, { method: 'post', body: JSON.stringify(sql) });
		//*-request de los datos en formato texto(viene todo el request)
		const data = await response.text();
		//*-se parsea solo la respuesta del Json enviada por el servidor.
		let array = JSON.parse(data);		
		console.log(array);		
		tablaProductos(array);
		//*-promesa de la funcion denguaje la ejecuto a la espera
		//*-de la respuesta del servidor.	
		const botones = await lenguaje();	
		
	} catch (error) {
		console.log('error en la conexion ', error);
	}
	
}
//*-productos
let tablaProductos = (arreglo) => {
	let tbody = document.getElementById('tablaBody');
	
	for (let i of arreglo) { 
		tbody.innerHTML +=
		`<tr>		   
		   <td>${i['codigo_barra']}</td>
		   <td>${i['nombre']}</td>
		   <td>${i['precio_instalacion']}</td>
		   <td>${i['precio_mayorista']}</td>
		   <td>${i['precio_venta']}</td>		  
		   <td><form method="POST" action="editar_productos_nuevos.php">
		   <button type="submit" class="btn btn-secondary" data-toggle="tooltip"
			data-placement="top" title="Editar" name="id" value=${i['id']}><i class="fas fa-edit" aria-hidden="true"></i></button></form></td>
			<td><form method="POST" action="traspasar_stock.php">
			<button type="submit" class="btn btn-primary" data-toggle="tooltip"
			 data-placement="top" title="Traspasar" name="id" value=${i['id']}><i class="fas fa-exchange-alt" aria-hidden="true"></i></button></form></td>
			<td ><button class="btn  btn-danger" data-toggle="tooltip" data-placement="top" title="Borrar" onclick=eliminarProducto(event,${i['id']})><i class="fa fa-trash" aria-hidden="true"></i></button></td>			
		 </tr>`
	 	
	}
	$('[data-toggle="tooltip"]').tooltip();

 }

 function lenguaje() {

	var f = new Date();
	var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();

	var table=$('#tablaProductos').DataTable({

		language: {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			"infoFiltered": "(Filtrado de _MAX_ total entradas)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ Entradas",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		},
		"aria": {
			"sortAscending": ": activate to sort column ascending",
			"sortDescending": ": activate to sort column descending"
		},
		"order": [[1, "desc"]],
		"stateSave":true
	});


     new $.fn.dataTable.Buttons(table, {
		buttons: [
			{
				extend: 'excelHtml5',
				title: 'ver_ventas' + fecha + ''
            }, {
				extend: 'pdfHtml5',
				title: 'ver_ventas' + fecha + ''
            }]

	});

	table.buttons(0, null).container().prependTo(
		table.table().container()
	);


}


function eliminarProducto(e, id) {
	e.preventDefault();
	var sql = 'DELETE from productos where id =' + id;
	$.ajax({
		type: 'POST',
		url: 'php/consulta.php',
		data: {
			sql: sql, tag: 'crud_datos' },
		success: function (data) {
			// console.log(data);
			if (data == 1) {
				alert('Borrado exitoso');
				cargar_productos(e);
			} else {
				alert('No Borrado');
			}
		},
		error: function (request, status, error) {
			alert("Error: Could not eliminarProducto");
		}
	});

}

window.onload = cargarCategoria
