<!DOCTYPE html>
<html lang="en">

<head>
	<title>Sistema View Point</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/main.css?vknet28">
    <!-- Font-icon css-->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css">
	<link rel="stylesheet" href="css/exportarBtn.css?vknet28">
</head>

<body class="app sidebar-mini rtl">
	<!-- Navbar-->
	<?php include "header.php"; ?>
	<?php include "left-menu.php"; ?>

	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>

	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-store"></i> Ver inventarios  </h1>
				<p>Ver inventarios y detalles</p>
				<?php date_default_timezone_set("America/Santiago"); setlocale(LC_ALL, "es_ES"); ?>
				<?php $fecha =strftime("%Y-%m-%d"); ?>
				<?php echo $fecha; ?>
			</div>
			<ul class="app-breadcrumb breadcrumb side">
				<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
				<li class="breadcrumb-item">Inventarios</li>
				<li class="breadcrumb-item active"><a href="#">Ver inventarios</a></li>
			</ul>
		</div>		<!--Codigo responsivo donde tengo la tabla-->
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body">

						<div class="my-3 p-3 bg-white rounded box-shadow">

							<h6 class="border-bottom border-gray pb-2 mb-0 ">Inventarios</h6>
							<br>
							<div class="form-row">
				<div class="form-group col-md-6">
				<!--<input type="date" class="form-control" id="fecha_inicio" onchange=cargar_onchange(NIVEL,ID_VENDEDORLOGUEADO) min="2013-01-01" max="2025-12-31" value="<?php echo date("Y-m-d");?>">-->
				</div>
			</div>
			<br>
							<table class="table table-striped  table-responsive" id="tablaVentas">
							<thead class="thead-dark">
							<tr>
							<th width="10%"> N° </th>
							<th width="10%"> Fecha </th>
							<th width="10%"> Productos</th>
							<th width="10%"> Cantidad ingresada</th>
							<th width="10%"> Estado</th>
							<th width="10%"></th>
							</tr>
							</thead>
							<tbody id="tablaBody"> </tbody>
							 </table>

						</div>
					</div>
				</div>
			</div>
		</div>


	</main>
	<!-- Essential javascripts for application to work-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
	<!-- The javascript plugin to display page loading on top-->
	<script src="js/plugins/pace.min.js"></script>
	<script type="text/javascript" src="js/ver_inventarios.js?vp5"></script>
	<script type="text/javascript" src="js/funciones.js?vp5"></script>
	<?php include "js/table.php" ?>



	<!-- Page specific javascripts-->
	<!-- Google analytics script-->
	<script type="text/javascript">
				var fecha_actual = '<?php echo $fecha ?>';
				var ID_VENDEDORLOGUEADO =<?php echo $idVendedor;?>;
				 var NIVEL =<?php echo $nivel;?>;
				console.error(fecha_actual);
				 window.onload = cargarInventarios(fecha_actual);


	</script>
</body>

</html>
