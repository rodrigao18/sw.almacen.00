<!DOCTYPE html>
<html lang="en">

<head>
<title>Sistema LAptop-PC</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vp5">
	<!-- Font-icon css-->
	
<link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">
</head>

<body class="app sidebar-mini rtl">
	<!-- Navbar-->
	<?php include "header.php"; ?>
	<?php include "left-menu.php"; ?>
	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>

	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fas fa-shopping-basket"></i> Retiro de dinero</h1>
				<p>TRetiro de dinero</p>
			</div>
			<ul class="app-breadcrumb breadcrumb side">
				<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
				<li class="breadcrumb-item">Usuarios</li>
				<li class="breadcrumb-item active"><a href="#">Retiro de dinero</a></li>
			</ul>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body"> </div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label>Laptop-PC</label>
							<input type="text" class="form-control" disabled id="laptop" value="Laptop-PC">
						</div>
						<div class="form-group col-md-4">
							<label>Retirar dinero</label>
							<div class="input-group mb-3">
								<input type="number" class="form-control" id="retiro" min="0" value="1">
						<div class="input-group-append">
							<button class="btn btn-primary" onclick="retirar(event)" type="button">Retirar dinero</button>
						</div>
					</div>
				</div>					
					</div>	
                    <div class="form-group">
                            <label for="comment">Observación:</label>
                                <textarea class="form-control" rows="3" id="obs"  value="sin observacion"></textarea>
                         </div>    				
					</div>
                    
                    					
				</div>
				<!--fin formrow-->					
				</div>
				
			</div>
		</div>
	</main>
	<!-- Essential javascripts for application to work-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
	<!-- The javascript plugin to display page loading on top-->
	<script src="js/plugins/pace.min.js"></script>
	<script type="text/javascript" src="js/retirar_dinero.js?vp5"></script>
	<script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
	<!-- Page specific javascripts-->
	<!-- Google analytics script-->
	<script type="text/javascript">
 		var ID_VENDEDOR =<?php echo $idVendedor;?>; 
        var ID_TURNO = <?php echo $idTurno;?>;
        var TIPO_TURNO = <?php echo $idTipoTurno;?>;
	</script>



</body>

</html>
