<!DOCTYPE html>
<html lang="en">

<head>
    <title>Ingresar ventas</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    <!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vp5">
    
	<link rel="stylesheet" type="text/css" href="css/ticket.css?vp5">
    <!-- Font-icon css-->
    
    <link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">
 
</head>

<body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <?php include "header.php"; ?>
    <?php include "left-menu.php"; ?>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>

    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-store-alt"></i> Ventas </h1>
                <p>Ingresar ventas</p>
            </div>
            <ul class="app-breadcrumb breadcrumb side">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item">Ventas</li>
                <li class="breadcrumb-item active"><a href="#">Ingresar ventas</a></li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                  <div class="float-right">
						<?php date_default_timezone_set("America/Santiago"); setlocale(LC_ALL,"es_ES");  echo "Fecha: ".strftime("%A %d de %B del %Y"); ?>
            <br>
						<?php echo "Hora: ".date("H:i:s"); ?>
            <br> <br>
					</div><br>
                    <div class="tile-body"> </div>
                    <div class="ml-5 mr-5  bg-white rounded box-shadow">

                    <a class="btn btn-success float-left mb-5" href="ver_reparaciones.php" target="_blanck" ><i class="fas fa-laptop"></i> Servicio técnico </a>
                    <br>
                    <br>
                    
                   <div class="input-group mb-2 mr-sm-2">
                            
                            <div class="input-group-prepend">
                                <div class="input-group-text"> <i class="fa fa-search" aria-hidden="true"></i></div>
                            </div>
                            <input class="form-control" id="buscar" name="buscar" onkeyup="prebusqueda('detalle');" type="text" placeholder="Nombre producto" aria-label="Search">
                        </div>
                        <br><br>
                       <!-- tabla producto-->
                        <div class="row">
                            <div class="col-md-12" id="tablaProductos">
                                <div class="">
                                    <div id="salidaTabla"></div>
                                </div>
                            </div>
                         </div>
                      
                     <br>           
                        <form method="POST" id="formularioGuardar">
                            <div class="input-group mb-2 mr-sm-2">
                            
                                <div class="input-group-prepend">
                                    <div class="input-group-text"> <i class="fa fa-search" aria-hidden="true"></i></div>
                                </div>
                                <input type="text" class="form-control" id="buscarCodigoBarra" onkeyup="buscarProductos(event);" placeholder="Ingrese codigo de barras">
                            </div>
                            <br>
                        </form>
                     <div id="nombreProducto"></div> 
                     <br>  
                    <div id="datos-productos">
                         <div class="form-row align-items-center">
                            <div class="col-auto">
                            <label>P. Instalación</label>                             
                             <input type="text" class="form-control mb-2" id="precioInstalacion" disabled placeholder="precio instalacion">
                            </div>
                        <div class="col-auto">
                            <div class="form-check inline pt-0">
                                <input class="form-check-input" type="checkbox" disabled id="checkI" onchange="cargarPrecio(1)">                                
                            </div>                        
                    </div>
                    <div class="form-row align-items-center">
                            <div class="col-auto"> 
                            <label>P. Mayorista</label>                                  
                             <input type="text" class="form-control mb-2" disabled id="precioMayorista" placeholder="precio mayorista">
                            </div>
                    </div>        
                        <div class="col-auto">
                            <div class="form-check inline pt-0">
                          <!-- <input type="checkbox"  data-toggle="toggle" disabled id="checkM" data-size="xs" onchange="cargarPrecio(2)">-->  
                           <input class="form-check-input" type="checkbox" disabled id="checkM" onchange="cargarPrecio(2)">              
                            </div>                        
                         </div>
                
                    <div class="form-row align-items-center">
                            <div class="col-auto">
                            <label>P. Venta</label>                                   
                             <input type="text" class="form-control mb-2" disabled id="precioVenta" placeholder="precio venta" >
                            </div>
                        <div class="col-auto">
                            <div class="form-check inline pt-0">
                              <input class="form-check-input" type="checkbox" disabled id="checkV" onchange="cargarPrecio(3)">                            
                        </div>                        
                    </div>
                    <div class="form-row align-items-center">
                            <div class="col-auto">
                            <label>Stock Laptop-PC</label>                                   
                             <input type="text" class="form-control mb-2" disabled id="stockb1" value="0" >
                            </div>
                            <div class="form-row align-items-center">
                            <div class="col-auto">
                            <label>Stock Celutronix</label>                                   
                             <input type="text" class="form-control mb-2" disabled id="stockb2" value="0" >
                            </div>                                          
                    </div>                    
                    </div>

                    </div>
                 </div>
                </div>


                </div>   
                    </div>
                    <!-- Fin del div de margenes -->
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body"> </div>
                    <div class="ml-5 mr-5  bg-white rounded box-shadow">
                        <form >

                            <table class="table table-striped">
                                <thead class="thead-dark">
                                    <tr>                                      
                                     
                                        <th scope="col" width="10%"> Item</th>
                                        <th scope="col" width="10%"> Código</th>
                                        <th scope="col" width="45%"> Nombre</th>
                                        <th scope="col" width="5%"> Cantidad </th>
                                        <th scope="col" width="15%"> Precio U.</th>
                                        <th scope="col" width="15%"> Precio Total.</th>
                                        <th scope="col" width="10%">  </th>
                                        <th scope="col" width="10%">Tipo</th>
                                        </tr>
                                    </thead>
                                <tbody id="tablaBodyVentas"></tbody>
                                </table>

                                <!-- TABLA TOTALNETO-->
                      					<table class="table table-striped" id="tablaTotal">
                      						<tbody id="valorTotal">
                      							<tr>
                      								<td colspan="3"></td>
                      								<td width="15%">Total </td>
                      								<td width="25%"><input type="text" class="form-control" id="total" disabled></td>
                      							</tr>
                      							<tr>
                      								<td colspan="3"></td>
                      								<td width="15%">Modo pago</td>
                      								<td width="25%"> <select class="form-control" id="selectModoPago" onchange="verificarMedioPago()">
                                              <option value="1"> EFECTIVO</option>
                                              <option value="2"> DEBITO</option>
                                              <option value="3"> CREDITO</option>
                                            </select></td>
                      							</tr>
                      							<tr>
                      								<td colspan="3"></td>
                      								<td width="15%">Monto Efectivo</td>
                      								<td width="25%"><input type="text" class="form-control" id="montoEfectivo" onkeyup="calcularVuelto()" ></td>
                      							</tr>

                                    <tr>
                                      <td colspan="3"></td>
                                      <td width="15%">Vuelto</td>
                                      <td width="25%"><input type="text" class="form-control" id="vuelto" disabled></td>
                                    </tr>
                                    <tr>
                                      <td colspan="3"></td>
                                      <td width="15%">Descuento</td>
                                      <td width="25%"><input type="number"  min="0"  max="99" class="form-control" id="descuento"  onkeyup="calcularDescuento(event,this)"></td>
                                    </tr>
                      						</tbody>
                      					</table>


							<br>
                        <div id ="conTicket">
 <table id="tablaTicket">
	<tbody id="Filaticket">
		<tr>
			<td id="TituloTicket" class="logo" colspan="3"></td>
		</tr>
		<tr>
			<td id="FechaTicket" class="FechaTicketClass" colspan="3"></td>
		</tr>
		<tr>
			<td id="VendedorTicket" class="VendedorTicketClass" colspan="3"></td>
		</tr>
		<tr>
			<th id="colNombre"></th>
            <th id="colCantidad"></th>
            <th id="colPrecio"></th>
		</tr>
		<tr>
			<td colspan="3"></td>
        </tr>
        <tr>
			<td></td>
			<td></td>
			<td></td>
		</tr>	
    </tbody>
    <tr>
            <th id="totalCompra"></td>
			<td></td>
            <td id="totalCompra_precio"></td>
            <td></td>
		</tr>		
		<tr>
			<td id="leyendaPie" class="leyendaPie" colspan="3">
        </td>
		</tr>
</table>            
                    
                            <br>
                            
							</div>
                            <button class="btn btn-primary float-right" onclick="finalizarVenta(event,'conTicket')"><i class="fa fa-check"></i> Finalizar venta </button>
                            <br><br>
                        </form>
                    </div>
                    <!-- Fin del div de margenes -->
                </div>
            </div>
        </div>



</div>

    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/ingresar_ventas.js?vp5"></script>
    <script type="text/javascript" src="js/editar.js"></script>
    <script type="text/javascript" src="js/funciones.js?vp5"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
   
    <script>
        <?php  $fecha  = date("Y-m-d") ?>;
        <?php $hora =   date("H:i:s") ?>;
        var FECHA = '<?php echo date("d-m-Y",strtotime($fecha)); ?>';
        var HORA = '<?php echo $hora ?>';
        var ID_VENDEDOR =<?php echo $idVendedor;?>; 
        var ID_TURNO = <?php echo $idTurno;?>;
     
        
		window.onload = ocultarTicket
    </script>

</body>

</html>
