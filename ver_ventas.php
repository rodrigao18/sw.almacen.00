<?php 	if (isset($_POST['fechaSet'])) {
}
        ?>



<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Ver ventas</title>
	<!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vknet28">
	<!-- Font-icon css-->
	<link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">

	<!--<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css">-->

	
</head>

<body class="app sidebar-mini rtl">
	<!-- Navbar-->
	<?php include "header.php"; ?>
	<?php include "left-menu.php"; ?>

	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>

	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-store"></i> Ver ventas  </h1>
				<p>Ver ventas y detalles</p>
				<?php date_default_timezone_set("America/Santiago"); setlocale(LC_ALL, "es_ES"); ?>
				<?php $fecha =strftime("%Y-%m-%d"); ?>
				<?php echo $fecha; 
				 $fecha_actual = date("d-m-Y");
				 ?>
			</div>
			<ul class="app-breadcrumb breadcrumb side">
				<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
				<li class="breadcrumb-item">Ventas</li>
				<li class="breadcrumb-item active"><a href="#">Ver ventas</a></li>
			</ul>
		</div>		<!--Codigo responsivo donde tengo la tabla-->
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body">

						<div class="my-3 p-3 bg-white rounded box-shadow">

							<h6 class="border-bottom border-gray pb-2 mb-0 ">Ventas</h6>
							<br>
							<div class="form-row">
							
				<div class="form-group col-md-6">
				<label> <i class="far fa-calendar-alt fa-2x"></i>&nbsp&nbsp Fecha Inicio</label>
					<input type="date" class="form-control" id="fecha_inicio" onchange=cargar_ventaInicio_onchange(NIVEL,ID_VENDEDORLOGUEADO) min="2013-01-01" max="2025-12-31" value="<?php echo date("Y-m-d",strtotime($fecha_actual."- 10 days"));?>">
				</div>
				<div class="form-group col-md-6">
				<label><i class="far fa-calendar-alt fa-2x"></i>&nbsp&nbsp  Fecha Término</label>
					<input type="date" class="form-control" id="fecha_termino" onchange=cargar_ventaInicio_onchange(NIVEL,ID_VENDEDORLOGUEADO) min="2013-01-01" max="2025-12-31" value="<?php echo date("Y-m-d");?>">
				</div>
			</div>
			</div>
			<br>
			<div id="salida">
							<table class="table table-striped" id="tablaVentas">
							<thead>					
							<tr>
								<th width="10%">N°</th>
								<th width="20%">Vendedor</th>
								<th width="20%">Fecha venta</th>
								<th width="20%">Hora venta</th>							
								<th width="20%">Total venta</th>							
								<th width="10%"></th>
								<th width="5%"> </th>
								
							</tr>
							</thead>					
						<tbody id="tablaBody">
						</tbody>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	</main>
	
	<!-- Essential javascripts for application to work-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>	
	<!-- The javascript plugin to display page loading on top-->
	<script src="js/plugins/pace.min.js"></script>	
	<script type="text/javascript" src="js/ver_ventas.js?vknet28"></script>
	<script type="text/javascript" src="js/funciones.js?vknet28"></script>
	<?php include "./js/table.php" ?>

	<script>
		<?php  $fecha  = date("Y-m-d") ?>;
		var fecha_actual = '<?php echo $fecha ?>';
		var fecha_actual_menos = '<?php echo date("Y-m-d",strtotime($fecha."- 10 days")); ?>';
		var ID_VENDEDORLOGUEADO = <?php echo $idVendedor;?>;
		var NIVEL = <?php echo $nivel;?>;
		window.onload = consultarVendedor(fecha_actual,fecha_actual_menos);
	</script>	

	

</body>

</html>
