<!DOCTYPE html>
<html lang="en">

<head>
	<title>Reparaciones</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Main CSS-->
    <!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vp5">
    
	<link rel="stylesheet" type="text/css" href="css/ticket.css?vp5">
    <!-- Font-icon css-->
    
    <link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">

</head>

<body class="app sidebar-mini rtl">
	<?php include "header.php"; ?>
	<?php include "left-menu.php"; ?>
	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>

	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-user-plus"></i> Editar reparacion </h1>
				<p>Editar reparacion</p>
                <?php date_default_timezone_set("America/Santiago"); setlocale(LC_ALL, "es_ES"); ?>
				<?php $fecha =strftime("%Y-%m-%d"); ?>
				<?php echo $fecha; 
				 $fecha_actual = date("d-m-Y");
				 ?>
			</div>
			<ul class="app-breadcrumb breadcrumb side">
				<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
				<li class="breadcrumb-item">Reparaciones</li>
				<li class="breadcrumb-item active"><a href="#">Editar reparacion</a></li>
			</ul>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body"> </div>

					<form method="POST" id="formularioGuardar">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label> <i class="far fa-calendar-alt fa-2x"></i>&nbsp&nbsp Fecha Ingreso </label>
					            <input type="date" class="form-control" id="fecha_inicio"  min="2013-01-01" max="2025-12-31" value="<?php echo date("Y-m-d",strtotime($fecha_actual));?>">
				        </div>
						<div class="form-group col-md-6">
                            <label> <i class="fas fa-wrench fa-2x"></i>&nbsp&nbsp Técnicos </label>
							<select class="form-control" id="select_tecnicos" name="select_tecnicos"></select>
				        </div>
                    </div>                
						
						<div class="form-row">
						<div class="form-group col-md-3">
								<label>Número de serie</label>
								<input type="text" class="form-control" id="serie" name="serie" placeholder="Ingrese serie" >
							</div>
							<div class="form-group col-md-3">
								<label>Tipo de equipo</label>
								<input type="text" class="form-control" id="tipo" name="tipo" placeholder="Ingrese tipo de equipo" onkeyup="this.value=mayusculas(this.value)" >
							</div>
							<div class="form-group col-md-3">
								<label>Marca</label>
								<input type="text" class="form-control" id="marca" name="marca" placeholder="Ingrese marca" onkeyup="this.value=mayusculas(this.value)" >
							</div>
							<div class="form-group col-md-3">
								<label>Modelo</label>
								<input type="text" class="form-control" id="modelo" name="modelo" placeholder="Ingrese modelo" onkeyup="this.value=mayusculas(this.value)" >
							</div>
						</div>			

						<div class="form-group">
                            <label for="comment">Accesorios:</label>
                                <textarea class="form-control" rows="3" id="accesorios"  value="sin accesorios"></textarea>
                         </div> 				
						 <div class="form-group">
                            <label for="comment">Falla:</label>
                                <textarea class="form-control" rows="3" id="falla"></textarea>
                         </div>
						 <div class="form-row">
                         <div class="form-group col-md-6">
								<label>Mano de obra</label>
								<input type="number" class="form-control" id="obra" name="obra" min="0" value="0">
							</div>		
							<div class="form-group col-md-6">
								<label>Entrega</label>
								<input type="number" class="form-control" id="entrega" name="entrega" min="0" value="0">
							</div>						
						</div>
                        <div class="form-row">
                         <div class="form-group col-md-6">
								<label>Repuesto</label>
								<input type="number" class="form-control" id="repuesto" name="repuesto" min="0" value="0">
							</div>		
							<div class="form-group col-md-6">
								<label>Saldo</label>
								<input type="number" class="form-control" id="saldo" name="saldo" min="0" value="0">
							</div>						
						</div>
                        <div class="form-row">
                        <div class="form-group col-md-6">
                            <label> <i class="far fa-calendar-alt fa-2x"></i>&nbsp&nbsp Fecha entrega </label>
					            <input type="date" class="form-control" id="fecha_entrega"  min="2013-01-01" max="2025-12-31" value="<?php echo date("Y-m-d",strtotime($fecha_actual));?>">
				        </div>
						<div class="form-group col-md-6">
                            <label><i class="fas fa-check fa-2x"></i>&nbsp&nbsp  </label>
								<select class="form-control" id="select_estado" name="select_estado">
								<option value="1">En Taller</option>
								<option value="2">Terminado</option>
								<option value="3">Entregado</option>
							</select>
				        	</div>					
                        </div>
						<br><br>
						<button class="btn btn-success float-right" onclick=editarReparaciones(event)><i class="fa fa-save"></i> Guardar reparación</button>
						<br><br>
					</form>

				</div>
			</div>
		</div>
	</main>
	<!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/editar_reparacion.js?vp6"></script>
    <script type="text/javascript" src="js/editar.js"></script>
    <script type="text/javascript" src="js/funciones.js?vp5"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
	<script type="text/javascript">
	var id = <?php echo $_POST['id']; ?>; //obtengo la variable del post php
		window.onload = cargarReparacion(id);

	</script>


</body>

</html>
