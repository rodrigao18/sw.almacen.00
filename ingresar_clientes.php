<!DOCTYPE html>
<html lang="en">

<head>
	<title>Sistema Cotización</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Main CSS-->
    <!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vp5">
    
	<link rel="stylesheet" type="text/css" href="css/ticket.css?vp5">
    <!-- Font-icon css-->
    
    <link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">

</head>

<body class="app sidebar-mini rtl">
	<?php include "header.php"; ?>
	<?php include "left-menu.php"; ?>
	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>

	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-user-plus"></i> Ingresar clientes </h1>
				<p>Ingresar clientes</p>
			</div>
			<ul class="app-breadcrumb breadcrumb side">
				<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
				<li class="breadcrumb-item">Clientes</li>
				<li class="breadcrumb-item active"><a href="#">Ingresar clientes</a></li>
			</ul>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body"> </div>

					<form method="POST" id="formularioGuardar">
						<div class="form-row">
							<div class="form-group col-md-6">
								<label>R.U.T</label>
								<input type="text" class="form-control" id="rutCliente" name="rutCliente" maxlength="9" placeholder=" Ej:123456789 (sin digito)" onfocus="this.value=sacarPuntosGuionRut(this.value)" onkeyup="this.value=soloRut(this.value)" onblur="this.value=validaRut(this.value,1)" >
							</div>
							<div class="form-group col-md-6">
								<label>Nombre</label>
								<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese nombre" onkeyup="this.value=mayusculas(this.value)" >
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Dirección</label>
								<input type="text" class="form-control" id="direccion" name="direccion" placeholder="Ingrese dirección" onkeyup="this.value=mayusculas(this.value)" >
							</div>
							<div class="form-group col-md-6">
								<label>Correo</label>
								<input type="text" class="form-control" id="correo" name="correo" placeholder="Ingrese correo" onkeyup="this.value=mayusculas(this.value)" >
							</div>			
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Celular</label>
								<input type="text" class="form-control" id="celular" name="celular" placeholder="Ingrese celular" onkeyup="this.value=mayusculas(this.value)" >
							</div>
                            <div class="form-group col-md-6">
								<label>Telefono</label>
								<input type="text" class="form-control" id="fono" name="fono" placeholder="Ingrese telefono" onkeyup="this.value=mayusculas(this.value)" >
							</div>                            							
						</div>		
                        <div class="form-group">
                            <label for="comment">Observaciones:</label>
                                <textarea class="form-control" rows="5" id="observaciones"></textarea>
                         </div>                       
					
							</div>
						</div>
						<!--Fin checkbok-->
						<br>
						<br><br>
						<button class="btn btn-primary float-right" onclick=GuardarCliente(event)><i class="fa fa-save"></i> Guardar cliente</button>
						<br><br>
					</form>

				</div>
			</div>
		</div>
	</main>
	<!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/ingresar_clientes.js?vp5"></script>
    <script type="text/javascript" src="js/editar.js"></script>
    <script type="text/javascript" src="js/funciones.js?vp5"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
	<script type="text/javascript">


	</script>


</body>

</html>
