<!DOCTYPE html>
<html lang="en">

<head>
	<title>Laptop-PC</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vknet28">
	<!-- Font-icon css-->
	<link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">

	<!--<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css">-->

	
</head>

<body class="app sidebar-mini rtl">
	<!-- Navbar-->
	<?php include "header.php"; ?>
	<?php include "left-menu.php"; ?>
	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>

	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-shopping-cart"></i> Ver reparaciones </h1>
				<p>Ver reparaciones, editar y eliminar</p>
			</div>
			<ul class="app-breadcrumb breadcrumb side">
				<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
				<li class="breadcrumb-item">Reparaciones</li>
				<li class="breadcrumb-item active"><a href="#">Ver reparaciones</a></li>
			</ul>
		</div>



		<!--Codigo responsivo donde tengo la tabla-->
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
					<div class="tile-body">
						<a style="margin-right: 15px" href="ver_clientes.php" class="btn btn-outline-primary float-right"><i class="icon fa fa-cart-plus"></i>Ir a Clientes</a>
						<div class="my-3 p-3 bg-white rounded box-shadow">

							<h6 class="border-bottom border-gray pb-2 mb-0 ">Reparaciones</h6>
							<br><br>
							<?php
							$filtrarVendedores='
							<div class="form-row">

							<div class="form-group col-md-6">
							<label>Filtrar Técnicos</label>
								<select onchange="filtroSelects()" class="form-control" id="select_tecnicos" name="select_tecnicos">

            					</select>
							</div>

							<div class="form-group col-md-6">
							<label>Filtrar Estado</label>
								<select onchange="filtroSelects()" class="form-control" id="select_estado" name="select_estado">
								<option value="0" selected="selected">Seleccion Estado</option>
								<option value="1">En taller</option>
								<option value="2">Terminado</option>
								<option value="3">Entregado</option>								
            					</select>
							</div>
							</div>'
							;
							$filtrarVentas= '<div class="form-group col-md-12">
							<label>Filtrar Ventas</label>
								<select onchange="filtroSelects()" class="form-control" id="selectVentas" name="ventas">
								<option value="0" selected="selected">Seleccione una venta</option>
								<option value="1">Cotizaciones</option>
								<option value="2">Nota Pedido</option>
								<option value="3">Venta</option>
								<option value="4">Nulas</option>
            					</select>
							</div>
							</div>';
							if($nivel==0){ echo $filtrarVendedores;}
							else{ echo $filtrarVentas;}
							?>
								<div id="salida">
								<div class="my-3 p-3 bg-white rounded box-shadow">
								
								<table class="table table-striped " id="tablaProductos">
								<thead>
								<tr>
								<th width="3%">Orden</th>
								<th width="10%">Fecha</th>	
								<th width="20%">Cliente</th>
								<th width="10%">Marca</th>
								<th width="10%">Módelo</th>							
								<th width="20%">Falla</th>
								<th width="10%">Estado</th>
								<th width="5%"> </th>
								<th width="5%"> </th>
								</tr>
								</thead>
								<tbody id="tablaBody"></tbody></table>

								</div>
							</div>
							</div>
					</div>
				</div>
			</div>
		</div>


	</main>
	<!-- Essential javascripts for application to work-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
	<!-- The javascript plugin to display page loading on top-->
	<script src="js/plugins/pace.min.js"></script>
	<script type="text/javascript" src="js/ver_reparaciones.js?vknet28"></script>
	<?php include "./js/table.php"; ?>



</body>

</html>
