<!DOCTYPE html>
<html lang="en">

<head>
    <title>Sistema Ventas</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css?vknet28">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css"href="fontawesome-5.5.0/css/all.min.css">
    <!-- style boton exporter -->
    <link rel="stylesheet" href="css/exportarBtn.css">
</head>

<body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <?php include "header.php"; ?>
    <?php include "left-menu.php"; ?>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>

    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fas fa-bookmark"></i> Ver categorias de gastos generales </h1>
                <p>Ver categorias, editar y eliminar</p>
            </div>
            <ul class="app-breadcrumb breadcrumb side">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item">Categorias</li>
                <li class="breadcrumb-item active"><a href="ver_vendedores.php">Ver categoria</a></li>
            </ul>
        </div>



        <!--Codigo responsivo donde tengo la tabla-->
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">
                        <a style="margin-right: 15px" href="ingresar_categoria.php" class="btn btn-outline-primary float-right"><i class="icon fa fa-cart-plus"></i>Ingresar Categorias</a>


                        <h6 class="border-bottom border-gray pb-2 mb-0 ">Categorias gastos generales</h6>


                        <div class="my-3 p-3 bg-white rounded box-shadow">
                            <div id="salida">
                                
                            </div>
                        </div>





                    </div>
                </div>
            </div>
        </div>


    </main>
    <!-- Essential javascripts for application to work-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/ver_categorias.js?vp5"></script>
    <!-- Data table plugin-->
    <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Page specific javascripts-->
    <!-- Google analytics script-->
    <script type="text/javascript">


    </script>
</body>

</html>
