<!DOCTYPE html>
<html lang="en">

<head>
	<title>Sistema Cotización</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="css/main.css?vknet28">
  <link rel="stylesheet" type="text/css" href="css/tablas_flujo.css">
	<!-- Font-icon css-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<!-- style boton exporter -->
	<link rel="stylesheet" href="css/exportarBtn.css?vknet25">
	
</head>

<body class="app sidebar-mini rtl">
	<!-- Navbar-->
	<?php include "header.php"; ?>
	<?php include "left-menu.php"; ?>

	<!-- Sidebar menu-->
	<div class="app-sidebar__overlay" data-toggle="sidebar"></div>

	<main class="app-content">
		<div class="app-title">
			<div>
				<h1><i class="fa fa-store"></i> Ver ventas </h1>
				<p>Ver ventas y detalles</p>
			</div>
			<ul class="app-breadcrumb breadcrumb side">
				<li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
				<li class="breadcrumb-item">Ventas</li>
				<li class="breadcrumb-item active"><a href="#">Ver ventas</a></li>
			</ul>
		</div>		<!--Codigo responsivo donde tengo la tabla-->
		<div class="row">
			<div class="col-md-12">
				<div class="tile">
<div class="tile-body">
<button class="btn  btn-default"  id="btn_atras" onclick="cargarTablasAtras()"><i class="fas fa-angle-left" aria-hidden="true"></i></button>
<button class="btn  btn-default float-right"  id="btn_adelante" onclick="cargarTablasProxima()"><i class="fas fa-angle-right" aria-hidden="true"></i></button>
<div class="my-3 p-3 bg-white rounded box-shadow">
<div id="tablas">
<h6 class="border-bottom border-gray pb-2 mb-0">Ventas <span id="año"></span></h6>
<br>
<?php
$meses=array("ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");
?>


<br>	
<!--button class="btn btn-info float-right" id="btn-finalizar" onclick="runTableSave(event)"><i class="fas fa-save"></i> Guardar resultados operacional</button>-->
<br>
<br>
<h6 class="border-bottom border-gray pb-2 mb-0">Detalles gastos generales </h6>
<br>
<!--Tabla detalle gastos generales-->
<table class="tablaResultadoOperacional table-responsive" id="tabla-detalles-gastos">
		<tr>
			<th>Gastos generales</th>
			<th>Enero </th>
			<th>Febrero</th>
			<th>Marzo</th>
			<th>Abril</th>
			<th>Mayo</th>
			<th>Junio</th>
			<th>Julio</th>
			<th>Agosto</th>
			<th>Septiembre</th>
			<th>Octubre</th>
			<th>Noviembre</th>
			<th>Diciembre</th>
			<th>Total item</th>
		</tr>
		<tbody id="tablaBodyDetalles">
		</tbody>
		<tr>
  <td class="encabezado-tabla-detalles">Total por mes</td>
    <?php 
    for($i=0; $i < 13 ; $i++){			
    echo '<td id="totalAcumuladoDetalle'.($i+1).'">0</td>';			
    }		
    ?> 
  </tr>
</table>




<br>
<br>
<h6 class="border-bottom border-gray pb-2 mb-0">Grafico gastos generales</h6>
<br>
<div class="row">
				<div class="col-md-12">
					<div class="tile">
						
						<div id="graficoBarrasGastosGenerales">
							<div class="embed-responsive embed-responsive-16by9">
								<canvas class="embed-responsive-item" id="barChartGastos" width="800" height="450"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
</div>
</div>
</div>
</div>
</div>
</div>


	</main>
	<!-- Essential javascripts for application to work-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
	<!-- The javascript plugin to display page loading on top-->
	<script src="js/plugins/pace.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
  <script type="text/javascript" src="js/editar_flujo.js?vknet25"></script>
 
  <script type="text/javascript" src="js/resultado_operacional.js?vknet28"></script>
  	
	<script type="text/javascript" src="js/plugins/bootstrap-notify.min.js"></script>
	<script type="text/javascript" src="js/funciones.js?vknet25"></script>
	<script type="text/javascript" src="js/dashboard_vendedor.js?vknet28"></script>
	<script type="text/javascript" src="js/html2canvas.min.js"></script>
	<script type="text/javascript" src="js/jquery.plugin.html2canvas.js"></script>
	<script>
		<?php  $fecha  = date("Y") ?>;
		var fecha_actual = '<?php echo $fecha ?>';
		window.onload = comprobarTabladetallesGenerales(fecha_actual);
	</script>
	<?php include "js/table.php" ?>



	<!-- Page specific javascripts-->
	<!-- Google analytics script-->

</body>

</html>
